package com.fertiletech.anchorsite.client.gui;

import java.util.LinkedHashMap;

public class ImageDestinationManager {
	
	public final static LinkedHashMap<String, String> IMAGE_DESTINATION_MAP = new LinkedHashMap<String, String>();
	
	//images ending with 2 are in black and white
	static
	{
		 //for lagosians
		IMAGE_DESTINATION_MAP.put("business2.jpg", "http://info.smeboutique.com/services/business-loans/bridge-finance");
		//for big traders
		IMAGE_DESTINATION_MAP.put("import.jpg", "http://info.smeboutique.com/services/business-loans/import-duty");
		 //for women
		IMAGE_DESTINATION_MAP.put("swec2.jpg", "http://info.smeboutique.com/swec");
		//for the classy
		IMAGE_DESTINATION_MAP.put("capital.jpg", "http://info.smeboutique.com/services/business-loans/working-capital");
		//for d smart guy
		IMAGE_DESTINATION_MAP.put("finance2.jpg", "http://info.smeboutique.com/services/advisory-services");
		//for the lonely
		IMAGE_DESTINATION_MAP.put("widows.jpg", "http://info.smeboutique.com/services/business-loans/widows-umbrella");
		//for the community
		IMAGE_DESTINATION_MAP.put("strenght2.jpg", "http://info.smeboutique.com/swec/membership");
		//for the office	
		IMAGE_DESTINATION_MAP.put("asset.jpg", "http://info.smeboutique.com/services/business-loans/asset-acquisition");
		//for the successful
		IMAGE_DESTINATION_MAP.put("passion2.jpg", "http://info.smeboutique.com/services/business-loans");
		//for the successful
		IMAGE_DESTINATION_MAP.put("expert.jpg", "http://info.smeboutique.com/contact-us");
		
	}
}
