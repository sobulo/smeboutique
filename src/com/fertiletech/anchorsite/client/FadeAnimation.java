package com.fertiletech.anchorsite.client;

import java.math.BigDecimal;

import com.google.gwt.animation.client.Animation;
import com.google.gwt.user.client.Element;

public class FadeAnimation extends Animation {

	private Element element;
	private FadeAnimationAlertCallback endCallBack;
	private double opacityIncrement;
	private double targetOpacity;
	private double baseOpacity;

	public FadeAnimation(Element element, FadeAnimationAlertCallback endCallBack) {
		this.element = element;
		this.endCallBack = endCallBack;
	}

	@Override
	protected void onUpdate(double progress) {
		element.getStyle()
				.setOpacity(baseOpacity + progress * opacityIncrement);
	}

	@Override
	protected void onComplete() {
		super.onComplete();
		element.getStyle().setOpacity(targetOpacity);
		if(endCallBack != null)
			endCallBack.fadeFinished();
	}

	public void fade(int duration, double targetOpacity) {
		if (targetOpacity > 1.0) {
			targetOpacity = 1.0;
		}
		if (targetOpacity < 0.0) {
			targetOpacity = 0.0;
		}
		this.targetOpacity = targetOpacity;
		String opacityStr = element.getStyle().getOpacity();
		try {
			baseOpacity = new BigDecimal(opacityStr).doubleValue();
			opacityIncrement = targetOpacity - baseOpacity;
			run(duration);
		} catch (NumberFormatException e) {
			// set opacity directly
			onComplete();
		}
	}

}
