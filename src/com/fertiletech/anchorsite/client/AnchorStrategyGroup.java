package com.fertiletech.anchorsite.client;

import com.fertiletech.anchorsite.client.gui.HomePage;
import com.fertiletech.anchorsite.client.gui.ImageSlider;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AnchorStrategyGroup implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public final static int MIN_Y = 115;
	public final static int MAX_Y = 460;

	public void onModuleLoad() {
		LayoutPanel p = RootLayoutPanel.get();
		final ImageSlider slider = new ImageSlider();
		HomePage home = new HomePage(slider);
		p.add(slider);
		p.add(home);
		trackPageview(Window.Location.getHref());
	}

	public static native void trackPageview(String url) /*-{
		$wnd._gaq.push([ '_trackPageview', url ]);
	}-*/;
}
