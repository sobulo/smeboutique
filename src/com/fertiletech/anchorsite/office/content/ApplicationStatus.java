package com.fertiletech.anchorsite.office.content;

import com.fertiletech.anchorsite.office.PanelUtilities;
import com.fertiletech.anchorsite.office.content.shell.CommentsPanel;
import com.fertiletech.anchorsite.office.util.ChangeFormStateHandler;
import com.fertiletech.anchorsite.office.util.DrivePickerDemo;
import com.fertiletech.anchorsite.office.util.SearchIDBox;
import com.fertiletech.anchorsite.shared.DTOConstants;
import com.fertiletech.anchorsite.shared.NPMBFormConstants;
import com.fertiletech.anchorsite.shared.TableMessage;
import com.fertiletech.anchorsite.shared.WorkflowStateInstance;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.Widget;

public class ApplicationStatus extends ResizeComposite{

	@UiField
	SearchIDBox idBox;
	@UiField
	Anchor formLink;
	@UiField
	Anchor formDownload;
	@UiField
	Anchor checkLink;
	@UiField
	Anchor checkDownload;
	@UiField
	Label formState;
	@UiField
	Label formMessage;
	@UiField
	Label formStateDisplay;
	@UiField
	Button changeServerState;
	@UiField
	CommentsPanel commentsPanel;
	@UiField
	DrivePickerDemo drivePickerPanel;
	
	ChangeFormStateHandler changeHander;
	final static String FORM_LINK = "javascript:;";
	final static String EDIT_BASE = GWT.getHostPageBaseURL() + "../apply/"; 
	//TODO remove dev edit form link, oauth now makes it impossible to use superdevmode to debug urgh!
	final static String EDIT_FORM_LINK = EDIT_BASE + "#apply";
	//String displayedLoanID = null;
	private AsyncCallback<TableMessage> loanStateCallBack = new AsyncCallback<TableMessage>() {

		@Override
		public void onFailure(Throwable caught) {
			PanelUtilities.errorBox.show(caught.getMessage());
			idBox.setText("");
			idBox.setEnabled(true);
		}

		@Override
		public void onSuccess(TableMessage result) {
			idBox.setEnabled(true);
			if(result == null)
			{	
				String[] id = idBox.getText().split(":");
				String msg = "Unable to find loan application with id ";
				if(id.length > 1)
					msg += id[0];
				PanelUtilities.errorBox.show(msg);
				idBox.setText("");
				return;
			}
			idBox.setText(Math.round(result.getNumber(DTOConstants.LOAN_STATUS_IDX)));
			String state = result.getText(DTOConstants.LOAN_STATUS_IDX);
			String stateDisplay = WorkflowStateInstance.valueOf(state).getDisplayString();
			formState.setText(state);
			formStateDisplay.setText(stateDisplay);
			formMessage.setText(result.getText(DTOConstants.LOAN_MESSAGE_IDX));
			changeServerState.setEnabled(true);
			Boolean[] formStates = NPMBFormConstants.getSubmissionValues(result);
			changePanelState(result, formStates[0]);
		}
	};
	
	private AsyncCallback<String[]> appDownloadLinkCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert(caught.getMessage());
			formDownload.setText("Error - refresh browser?");
		}

		@Override
		public void onSuccess(String[] result) {
			setupDownloadLink("Download Submitted Application", result[0], formDownload);
			setupDownloadLink("Download Checklist", result[1], checkDownload);
		}
	};
	
	private static ApplicationStatusUiBinder uiBinder = GWT
			.create(ApplicationStatusUiBinder.class);

	interface ApplicationStatusUiBinder extends
			UiBinder<Widget, ApplicationStatus> {
	}

	public ApplicationStatus() {
		initWidget(uiBinder.createAndBindUi(this));
		idBox.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				searchLoanID();
			}
		});
		changeHander = new ChangeFormStateHandler(formState, formStateDisplay, formMessage);
		changeServerState.addClickHandler(changeHander);				
	}
	
	private void setupDownloadLink(String text, String href, Anchor a)
	{
		a.setText(text);
		a.setHref(href);
		a.setEnabled(true);
		a.setTarget("_blank");
	}
	
	public void setLoanID(String ID)
	{
		drivePickerPanel.update(null, true);
		clear();
		idBox.setText("Loading, please wait ...");
		idBox.setEnabled(false);
		changeServerState.setEnabled(false);
		changeHander.loanID = null;
		commentsPanel.clear();		
		if(ID == null || ID.trim().length() == 0) return;
		PanelUtilities.getLoanMktService().getLoanState(ID, loanStateCallBack);
	}
	
	private void searchLoanID()
	{
		Long id = idBox.getValue();
		if(id == null)
		{
			PanelUtilities.errorBox.show(idBox.getText() + " [id should consist of numbers only]");
			return;
		}		
		drivePickerPanel.update(null, true);
		clear();
		idBox.setEnabled(false);
		changeServerState.setEnabled(false);
		changeHander.loanID = null;
		commentsPanel.clear();		
		idBox.setText(id + ": searching, please wait ...");		
		PanelUtilities.getLoanMktService().getLoanState(id, loanStateCallBack);
	}
	
	private void killAnchor(Anchor a)
	{
		a.setHref("javascript:;");
		a.setEnabled(false);
	}
	
	
	private void changePanelState(TableMessage loanState, boolean appSubmitted)
	{
		String appFormID = loanState.getText(DTOConstants.LOAN_KEY_IDX);
		changeHander.loanID = loanState.getMessageId();
		drivePickerPanel.update(changeHander.loanID, true);
		killAnchor(checkLink);
		checkLink.setText("");
		commentsPanel.setCommentID(changeHander.loanID);
		setupDownloadLink("View/Edit Application Form", EDIT_FORM_LINK + "/" + changeHander.loanID, formLink);
		if(appSubmitted)
		{
			killAnchor(formDownload);
			formDownload.setText("Fetching document, please wait ...");	
			PanelUtilities.getLoanMktService().getApplicationFormDownloadLink(appFormID, appDownloadLinkCallback);
		}
		else
		{
			setupDownloadLink("Not yet submitted/available", FORM_LINK, checkDownload);
			setupDownloadLink("Not yet submitted/available", FORM_LINK, formDownload);
		}			
 	}
	
	private void clear()
	{
		checkLink.setText("");
		formLink.setText("");
		killAnchor(checkLink);
		killAnchor(formLink);
		setupDownloadLink("Select/search for an application first", FORM_LINK, checkDownload);
		setupDownloadLink("Select/search for an application first", FORM_LINK, formDownload);
	}
}
