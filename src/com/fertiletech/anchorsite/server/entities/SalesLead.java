/**
 * 
 */
package com.fertiletech.anchorsite.server.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.anchorsite.shared.WorkflowStateInstance;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class SalesLead{
	
	@Id
	long key;
	
	//customer fields -- potential index candidates, for now useful to have these without having to load forms
	String phoneNumber;
	String fullName;
	Double loanAmount;
	Integer tenor;
	String companyName;
	String displayMessage;
	
	
	WorkflowStateInstance currentState;
	
	//indexed fields
	@Indexed String bankLoanId;
	@Indexed Date dateCreated;
	@Indexed Long keyCopy;
	@Indexed String loanType; 
	@Indexed String bizSector;

	
	//auto-populated fields
	Date dateUpdated; 
	String updateUser;	
	
	@Parent Key<ApplicantUniqueIdentifier> parentKey;
	
	public String getEmail(){
		return parentKey.getName();
	}
	

	public String getBankLoanId() {
		return bankLoanId;
	}

	void setBankLoanId(String bankLoanId) {
		this.bankLoanId = bankLoanId;
	}

	public Boolean getLoanApproved() {
		return currentState.isApprovedState();
	}

	public SalesLead() {} //empty constructor required by objectify
	
	public SalesLead(String email, long id)
	{
		this.parentKey = new Key<ApplicantUniqueIdentifier>(ApplicantUniqueIdentifier.class, email);
		this.currentState = WorkflowStateInstance.APPLICATION_STARTED;
		this.dateCreated = new Date(); 	
		this.key = id;
	}
	
	void setDecisionFields(
			String phoneNumber,
			String fullName,
			String bizSector,
			String loanType,
			String compName,
			Integer tenor,
			Double amount
	)
	{
		this.phoneNumber = phoneNumber;
		this.fullName = fullName;
		this.bizSector = bizSector;
		this.loanType = loanType;
		this.companyName = compName;
		this.tenor = tenor;
		this.loanAmount = amount;
	}
	
	SalesLead makeCopy(long id)
	{
		if(!allowNewSiblings()) throw new RuntimeException("Copying this lead is not allowed. Contact support for assistance");
		SalesLead copy = new SalesLead(parentKey.getName(), id);
		copy.setDecisionFields(phoneNumber, fullName, bizSector, loanType, companyName, null, null);
		return copy;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
			
	public String getFullName() {
		return fullName;
	}	
			


	public Date getDateCreated() {
		return dateCreated;
	}
	
	public Date getDateUpdated() {
		return dateUpdated;
	}
	
	public long getLeadID()
	{
		return key;
	}
	
	@PrePersist
	void changeDateUpdated() {
		this.dateUpdated = new Date();
	}
	
	String getUpdateUser() {
		return updateUser;
	}
	
	public Key<SalesLead> getKey()
	{
		return new Key<SalesLead>(parentKey, SalesLead.class, key);
	}

	public WorkflowStateInstance getCurrentState() {
		return currentState;
	}


	public void setCurrentState(WorkflowStateInstance state) {
		this.currentState = state;
	}
	
	public String getDisplayMessaage()
	{
		return displayMessage;
	}
	
	void setDisplayMessage(String msg)
	{
		displayMessage = msg;
	}
	
	public boolean allowNewSiblings()
	{
		Calendar oneYearAgo = GregorianCalendar.getInstance();
		oneYearAgo.roll(Calendar.YEAR, false);
		switch (currentState) 
		{
			case APPLICATION_APPROVED:
			case APPLICATION_CANCEL:
				return true;
			case APPLICATION_DENIED:
				return oneYearAgo.after(dateCreated);
			default:
				return false;
		}
	}
	
	@PrePersist
	void copyMyKeyForIndexing()
	{
		if(keyCopy == null)
			keyCopy = key;
	}
}