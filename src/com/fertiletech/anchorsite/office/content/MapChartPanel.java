package com.fertiletech.anchorsite.office.content;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.fertiletech.anchorsite.base.MyAsyncCallback;
import com.fertiletech.anchorsite.office.PanelUtilities;
import com.fertiletech.anchorsite.shared.DTOConstants;
import com.fertiletech.anchorsite.shared.TableMessage;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ChartWidget;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.Selection;
import com.googlecode.gwt.charts.client.event.SelectEvent;
import com.googlecode.gwt.charts.client.event.SelectHandler;
import com.googlecode.gwt.charts.client.map.Map;
import com.googlecode.gwt.charts.client.map.MapOptions;
import com.googlecode.gwt.charts.client.options.MapType;
import com.googlecode.gwt.charts.client.options.Options;

public class MapChartPanel extends MortgageChartPanel {
	private final String KEY = "AIzaSyAHjN9fKIbD8tb6iiB_SP87lgiIhUae54Y";
	protected ChartPackage getChartType() {
		return ChartPackage.MAP;
	}
	PopupPanel selectionDisplay;
	HashMap<Integer, String> selectionMap;
	
	public MapChartPanel() {
		super();
		selectionDisplay = new PopupPanel(true);
		selectionMap = new HashMap<Integer, String>();
		selectionDisplay.setSize("300px", "300px");
	}
	
	private Integer getSelectionKey(Integer row, Integer col)
	{
		Window.alert("Got Row: " + row + " Col: " + col);
		return row;
	}
	
	private void showSelections(Selection s)
	{
		String message = s==null? "Multiple selections not supported" : selectionMap.get(getSelectionKey(s.getRow(), s.getColumn()));
		if(message == null)
			message = "No details available for this selection";
		selectionDisplay.clear();
		selectionDisplay.add(new HTML(message));
		selectionDisplay.show();
	}

	protected ChartWidget<? extends Options> getChart() {
		final Map map = new Map();
		MapOptions options = MapOptions.create();
		options.setShowTip(true);
		options.setMapType(MapType.NORMAL);
		options.setUseMapTypeControl(true);
		
		opt = options;
		return map;
	}
	
	protected String getKey()
	{
		return KEY;
	}

	protected void drawChart(final Date startDate, final Date endDate) {
		final Map mapChart = (Map) chart;
		final ListBox categoryTypes = new ListBox();
		final CheckBox duplicateBox = new CheckBox("Include Duplicates");
		footer.clear();
		HorizontalPanel catPanel = new HorizontalPanel();
		catPanel.setSpacing(10);
		catPanel.add(new Label("Type: "));
		catPanel.add(categoryTypes);
		catPanel.add(duplicateBox);
		footer.add(catPanel);
		container.setWidgetSize(footer, 50);
		container.animate(3000);
		MyAsyncCallback<List<TableMessage>> mapCallBack = new MyAsyncCallback<List<TableMessage>>() {

			private String getSelectedCategory() {
				return categoryTypes.getValue(categoryTypes.getSelectedIndex());
			}

			private void drawMap(List<TableMessage> catList, Boolean showDuplicates) {
				DataTable data = getTable(catList, showDuplicates);
				mapChart.draw(data, (MapOptions) opt);				
				mapChart.setTitle("Displaying " + data.getNumberOfRows() + " entries for [" + getSelectedCategory() + "]");
			}

			@Override
			public void onSuccess(List<TableMessage> result) {
				// Prepare the data
				if (result.size() == 0) {
					PanelUtilities.errorBox.show("No results found. Try widening specified date range");
					return;
				}

				final HashMap<String, List<TableMessage>> categoryMap = categorizeData(result);
				categoryTypes.clear();
				int largestCategory = 0;
				int count = 0;
				int initIdx = 0;
				for (String type : categoryMap.keySet()) {
					if (categoryMap.get(type).size() > largestCategory) {
						largestCategory = categoryMap.get(type).size();
						initIdx = count;
					}
					categoryTypes.addItem(type);
					count++;
				}
				categoryTypes.addChangeHandler(new ChangeHandler() {

					@Override
					public void onChange(ChangeEvent event) {
						drawMap(categoryMap.get(getSelectedCategory()), duplicateBox.getValue());
					}
				});
				categoryTypes.setSelectedIndex(initIdx);
				duplicateBox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
					
					@Override
					public void onValueChange(ValueChangeEvent<Boolean> event) {
						drawMap(categoryMap.get(getSelectedCategory()), event.getValue());
					}
				});
				
				drawMap(categoryMap.get(getSelectedCategory()), duplicateBox.getValue());
			}

			@Override
			public void onFailure(Throwable caught) {
				PanelUtilities.errorBox.show("Error occured on SME B cloud. Details: " + caught.getMessage());
			}

			@Override
			protected void callService(AsyncCallback<List<TableMessage>> cb) {
				PanelUtilities.getLoginService().getMapAttachments(startDate, endDate, cb);
			}
		};
		mapChart.addSelectHandler(new SelectHandler() {
			
			@Override
			public void onSelect(SelectEvent event) {
				JsArray<Selection> selections = mapChart.getSelection();
				if(selections == null)
					return;
				if(selections.length() > 1)
				{
					showSelections(null);
				}
				else
					showSelections(selections.get(0));
				
			}
		});
		mapCallBack.go("Fetching map data, please wait ...");
	}

	private HashMap<String, List<TableMessage>> categorizeData(List<TableMessage> locations) {
		HashMap<String, List<TableMessage>> result = new HashMap<String, List<TableMessage>>();
		for (TableMessage loc : locations) {
			String cat = loc.getText(DTOConstants.CATEGORY_IDX);
			List<TableMessage> catLocations = result.get(cat);
			if (catLocations == null)
				catLocations = new ArrayList<TableMessage>();
			catLocations.add(loc);
			result.put(cat, catLocations);
		}
		return result;
	}

	NumberFormat latLongFormat = NumberFormat.getFormat("#.######");
	private DataTable getTable(List<TableMessage> result, Boolean showDuplicates) {
		selectionMap.clear();
		HashSet<String> uniqueSet = new HashSet<String>();
		DataTable dataTable = DataTable.create();
		dataTable.addColumn(ColumnType.NUMBER, "A");
		dataTable.addColumn(ColumnType.NUMBER, "B");
		dataTable.addColumn(ColumnType.STRING, "Home");
		
		dataTable.addRows(result.size());
		for (int i = 0; i < result.size(); i++) {
			double lat = result.get(i).getNumber(DTOConstants.LAT_IDX);
			double lng = result.get(i).getNumber(DTOConstants.LNG_IDX);
			if(showDuplicates == null || !showDuplicates)
			{
				;
				String key = latLongFormat.format(lat) + "-" + latLongFormat.format(lng);
				if (uniqueSet.contains(key))
					continue;
				else
					uniqueSet.add(key);
			}
			String htmlMessage = result.get(i).getText(DTOConstants.DETAIL_IDX);
			String detail = new HTML(htmlMessage).getText();
			
			//add to dataset
			dataTable.addRow();
			dataTable.setValue(i, 0, lat);
			dataTable.setValue(i, 1, lng);
			dataTable.setValue(i, 2, detail);
			selectionMap.put(i, htmlMessage);
			
		}
		return dataTable;
	}
}
