package com.fertiletech.anchorsite.apply.forms;

import java.util.ArrayList;
import java.util.HashMap;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.TabListItem;
import org.gwtbootstrap3.client.ui.TabPane;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.client.ui.html.Small;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;
import org.gwtbootstrap3.extras.bootbox.client.callback.ConfirmCallback;

import com.fertiletech.anchorsite.apply.CustomerAppHelper;
import com.fertiletech.anchorsite.apply.NameTokens;
import com.fertiletech.anchorsite.apply.formwidgets.BSControlValidator;
import com.fertiletech.anchorsite.apply.formwidgets.BSLoanFieldValidator;
import com.fertiletech.anchorsite.apply.formwidgets.BSTextWidget;
import com.fertiletech.anchorsite.shared.DTOConstants;
import com.fertiletech.anchorsite.shared.FormValidator;
import com.fertiletech.anchorsite.shared.NPMBFormConstants;
import com.fertiletech.anchorsite.shared.WorkflowStateInstance;
import com.fertiletech.anchorsite.shared.oauth.ClientUtils;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class MortgageApplicationFormPanel extends Composite {

	@UiField
	TabListItem personalHeader;
	@UiField
	TabListItem educationHeader;
	@UiField
	TabListItem familyHeader;
	@UiField
	TabListItem signatureHeader;
	@UiField
	TabPane personalBody;
	@UiField
	TabPane educationBody;
	@UiField
	TabPane familyBody;
	@UiField
	TabPane signatureBody;

	// personal fields
	@UiField
	BSTextWidget<String> surname;
	@UiField
	BSTextWidget<String> firstName;

	@UiField
	BSTextWidget<String> phone;
	@UiField
	BSTextWidget<String> email;
	@UiField
	BSTextWidget<String> bvn;
	
	//company fields
	@UiField
	BSTextWidget<String> compName;
	@UiField
	BSTextWidget<String> compNo;
	@UiField
	BSTextWidget<String> compAddress;

	@UiField
	BSTextWidget<String> bizSector;
	@UiField
	BSTextWidget<String> compWeb;
	@UiField
	BSTextWidget<String> heardAbout;	
	
	//loan
	@UiField
	BSTextWidget<String> loanAmount;
	@UiField
	BSTextWidget<String> tenor;

	@UiField
	BSTextWidget<String> repay;
	@UiField
	BSTextWidget<String> project;
	@UiField
	BSTextWidget<String> loanType;		


	// enrollment fields

	// declaration fields
	@UiField
	Paragraph declarationContent;
	@UiField
	Small declarationAuthor;

	@UiField
	Button personalSave;
	@UiField
	Button familySave;
	@UiField
	Button educationSave;
	@UiField
	Button submit;

	String loanId = null;

	HashMap<Button, WorkflowStateInstance> buttonToStateMap = new HashMap<Button, WorkflowStateInstance>();
	// Create an asynchronous callback to handle the result.
	final AsyncCallback<HashMap<String, String>> fetchFormDataCallback = new AsyncCallback<HashMap<String, String>>() {

		@Override
		public void onSuccess(final HashMap<String, String> result) {
			try {
				setFormData(result);
				setFormState(result);
			} catch (Exception e) {
				StringBuilder errorMessage = new StringBuilder("<b>Warning, a few fields might not have "
						+ "been loaded: " + e.getMessage() + "<b><hr><div style='font-size:smaller'><ul>");
				for (String key : result.keySet())
					errorMessage.append("<li>").append(result.get(key)).append("</li>");
				errorMessage.append("</ul></div>");
				CustomerAppHelper.showErrorMessage(errorMessage.toString());

			}
		}

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper.showErrorMessage("Unable to retrieve application form. Try refreshing your browser. "
					+ "Contact info@smeboutique.com if problems persist. <p> Error msg: <b>" + caught.getMessage()
					+ "</b></p>");
			enableButtons(false);
		}
	};

	private void setFormState(HashMap<String, String> result)
	{
		String setEmail = email.getValue();
		loanId = result.get(NPMBFormConstants.ID);
		boolean editable = Boolean.valueOf(result.get(NPMBFormConstants.ID_EDIT));
		WorkflowStateInstance state = WorkflowStateInstance
				.valueOf(result.get(NPMBFormConstants.ID_STATE));
		enableTabs(state);
		activatePanel(state);
		// override editable, if an email set (assumption is
		// this email set only if NOT ops/staff)
		if (editable)
			if (setEmail.trim().length() > 0
					&& !setEmail.equalsIgnoreCase(result.get(NPMBFormConstants.EMAIL))) {
				CustomerAppHelper.showInfoMessage(
						"This app was created from a different email address, setting to read only mode. Login with "
								+ setEmail + " and/or logout to edit the application form");
				editable = false;
			}
		enableEditing(editable);		
	}
	private AsyncCallback<String[]> submitCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			Bootbox.alert("Unable to submit application, please try again. Error was: " + caught.getMessage());
			enableButtons(true);
		}

		@Override
		public void onSuccess(String[] result) {
			Bootbox.alert(
					"You application form has been submitted. If you need to make edits to the submitted application, please contact support");
			boolean editable = Boolean.valueOf(result[DTOConstants.LOAN_EDIT_IDX]);
			enableEditing(editable);
			nextPanel(WorkflowStateInstance.APPLICATION_CONFIRM);
			CustomerAppHelper.updateLatLngVals(loanId);
		}
	};

	private AsyncCallback<String[]> saveCallback = new AsyncCallback<String[]>() {

		@Override
		public void onFailure(Throwable caught) {
			Bootbox.alert("Unable to save values. Error was: " + caught.getMessage());
			enableButtons(true);
		}

		@Override
		public void onSuccess(String[] result) {
			Bootbox.alert("Saved successfully. You may continue editing your application");
			enableButtons(true);
			WorkflowStateInstance state = WorkflowStateInstance.valueOf(result[DTOConstants.LOAN_STATE_IDX]);
			nextPanel(state);
			CustomerAppHelper.updateLatLngVals(loanId);
		}
	};

	private static MortgageApplicationFormPanelUiBinder uiBinder = GWT
			.create(MortgageApplicationFormPanelUiBinder.class);

	interface MortgageApplicationFormPanelUiBinder extends UiBinder<Widget, MortgageApplicationFormPanel> {
	}

	public MortgageApplicationFormPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		buttonToStateMap.put(personalSave, WorkflowStateInstance.APPLICATION_PERSONAL);
		buttonToStateMap.put(familySave, WorkflowStateInstance.APPLICATION_LOAN);
		buttonToStateMap.put(submit, WorkflowStateInstance.APPLICATION_CONFIRM);
		buttonToStateMap.put(educationSave, WorkflowStateInstance.APPLICATION_COMPANY);
		FormUtils.setupListBox(bizSector, NPMBFormConstants.BIZ_CATEGORIES);
		FormUtils.setupListBox(tenor, 1, 12, "month", "months");
		FormUtils.setupListBox(loanType, NPMBFormConstants.LOAN_TYPES);

		ValueChangeHandler<String> declarationHandler = new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				setDeclarationAuthor(getFormData());

			}
		};

		surname.addValueChangeHandler(declarationHandler);
		firstName.addValueChangeHandler(declarationHandler);

		declarationContent.setText(NPMBFormConstants.DECLARATION);


		// setup click handlers
		ClickHandler saveHandler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				submitHandler((Button) event.getSource());
			}
		};

		ClickHandler submitHandler = new ClickHandler() {

			@Override
			public void onClick(final ClickEvent event) {
				final Button clickSource = (Button) event.getSource();
				String msg = "Click ok to confirm you have read and agree with the terms stated on this page. "
						+ "Please note that you will not be able to edit your application details after this step (without contacting our customer support team)";
				Bootbox.confirm(msg, new ConfirmCallback() {

					@Override
					public void callback(boolean result) {
						if (result)
							submitHandler(clickSource);
					}
				});

			}
		};

		setupValidators();
		personalSave.addClickHandler(saveHandler);
		familySave.addClickHandler(saveHandler);
		educationSave.addClickHandler(saveHandler);
		submit.addClickHandler(submitHandler);
	}

	public void enableTabs(WorkflowStateInstance state) {
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setEnabled(true);
		case APPLICATION_LOAN:
			familyHeader.setEnabled(true);
		case APPLICATION_COMPANY:
			educationHeader.setEnabled(true);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setEnabled(true);
		}
	}

	public void activatePanel(WorkflowStateInstance state) {
		deactivatePanels();
		switch (state) {
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		case APPLICATION_CONFIRM:
			signatureHeader.setActive(true);
			signatureBody.setActive(true);
			break;
		case APPLICATION_LOAN:
			familyHeader.setActive(true);
			familyBody.setActive(true);
			break;
		case APPLICATION_COMPANY:
			educationHeader.setActive(true);
			educationBody.setActive(true);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			personalHeader.setActive(true);
			personalBody.setActive(true);
		}
	}

	public void deactivatePanels() {
		signatureHeader.setActive(false);
		signatureBody.setActive(false);
		familyHeader.setActive(false);
		familyBody.setActive(false);
		educationHeader.setActive(false);
		educationBody.setActive(false);
		personalHeader.setActive(false);
		personalBody.setActive(false);
	}

	public void priorPanel(WorkflowStateInstance state) {
		switch (state) {
		case APPLICATION_CONFIRM:
			switchPanel(signatureHeader, familyHeader, signatureBody, familyBody);
			break;
		case APPLICATION_LOAN:
			switchPanel(familyHeader, educationHeader, familyBody, educationBody);
			break;
		case APPLICATION_COMPANY:
			switchPanel(educationHeader, personalHeader, educationBody, personalBody);
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		}
	}

	public void nextPanel(WorkflowStateInstance state) {
		boolean allowJump = !ClientUtils.NPMBUserCookie.getCookie().isOps();
		switch (state) {
		case APPLICATION_CONFIRM:
			if (allowJump)
				History.newItem(NameTokens.PRINT + "/" + loanId);
			break;
		case APPLICATION_LOAN:
			switchPanelToNext(familyHeader, signatureHeader, familyBody, signatureBody);
			break;
		case APPLICATION_COMPANY:
			switchPanelToNext(educationHeader, familyHeader, educationBody, familyBody);
			break;
		case APPLICATION_PERSONAL:
		case APPLICATION_STARTED:
			switchPanelToNext(personalHeader, educationHeader, personalBody, educationBody);
			break;
		case APPLICATION_APPROVED:
		case APPLICATION_PENDING:
		case APPLICATION_CANCEL:
		case APPLICATION_DENIED:
		}
	}

	private void switchPanel(TabListItem currentHeader, TabListItem switchedHeader, TabPane currentBody,
			TabPane switchedBody) {
		currentHeader.setActive(false);
		currentBody.setActive(false);
		switchedHeader.setActive(true);
		switchedBody.setActive(true);
	}

	private void switchPanelToNext(TabListItem currentHeader, TabListItem nextHeader, TabPane currentBody,
			TabPane nextBody) {
		if (!currentHeader.isActive()) // something else is active, and yes this
										// is a hack
			deactivatePanels();
		switchPanel(currentHeader, nextHeader, currentBody, nextBody);
		nextHeader.setEnabled(true);
	}

	public void submitHandler(Button b) {
		HashMap<String, String> customerInfo = getFormData();
		customerInfo.put(NPMBFormConstants.ID_STATE, buttonToStateMap.get(b).toString());
		if (validateFields(b))
			submitFormData(customerInfo, b);
	}

	public void enableButtons(boolean enabled) {
		personalSave.setEnabled(enabled);
		educationSave.setEnabled(enabled);
		familySave.setEnabled(enabled);
		submit.setEnabled(enabled);
	}

	private void submitFormData(HashMap<String, String> data, Button b) {
		GWT.log("Loan ID: " + loanId);
		enableButtons(false); // prevent user from hitting save multiple times
								// while waiting on server
		if (b == submit) {
			String category = data.get(NPMBFormConstants.LOAN_TYPE);
			String action = data.get(NPMBFormConstants.BIZ_SECTOR);
			CustomerAppHelper.trackEvent(category, action, loanId);
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, loanId, true, submitCallback);
		} else {
			CustomerAppHelper.trackEvent("Edit Application Form", b.getText(), loanId);
			CustomerAppHelper.LOAN_MKT_SERVICE.saveLoanApplicationData(data, loanId, false, saveCallback);
		}
	}

	private boolean validateFields(Button button) {
		errorMap.clear();

		ArrayList<BSControlValidator> validators;
		if (button == personalSave)
			validators = personalValidators;
		else if (button == educationSave)
			validators = educationValidators;
		else if (button == familySave) {
			validators = familyValidators;
		} else {
			validators = new ArrayList<BSControlValidator>();
			validators.addAll(personalValidators);
			validators.addAll(educationValidators);
			validators.addAll(familyValidators);
		}

		for (BSControlValidator v : validators)
			v.markErrors();

		if (errorMap.size() > 0) {
			StringBuilder b = new StringBuilder(
					errorMap.size() + (errorMap.size() == 1 ? " error" : " errors") + " found.<ul>");
			for (String val : errorMap.values())
				b.append("<li>").append(val).append("</li>");
			b.append("</ul>");
			CustomerAppHelper.showErrorMessage(b.toString());
		}
		// Window.alert("Validation complete: " + errorMap.size());
		return errorMap.size() == 0;
	}

	ArrayList<BSControlValidator> personalValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> educationValidators = new ArrayList<BSControlValidator>();
	ArrayList<BSControlValidator> familyValidators = new ArrayList<BSControlValidator>();
	HashMap<String, String> errorMap = new HashMap<String, String>();

	HashMap<String, BSTextWidget> controlWidgetsMap = new HashMap<String, BSTextWidget>();

	private void setupValidators() {
		// personal validators
		controlWidgetsMap.put(NPMBFormConstants.EMAIL, email);
		controlWidgetsMap.put(NPMBFormConstants.TEL_NO, phone);
		controlWidgetsMap.put(NPMBFormConstants.SURNAME, surname);
		controlWidgetsMap.put(NPMBFormConstants.FIRST_NAME, firstName);
		controlWidgetsMap.put(NPMBFormConstants.BVN, bvn);
		
		//sme validators
		controlWidgetsMap.put(NPMBFormConstants.COMPANY_NAME, compName);
		controlWidgetsMap.put(NPMBFormConstants.COMPANY_WEBSITE, compWeb);
		
		//loan validators
		controlWidgetsMap.put(NPMBFormConstants.LOAN_AMOUNT, loanAmount);
		controlWidgetsMap.put(NPMBFormConstants.REPAY_SOURCE, repay);
		controlWidgetsMap.put(NPMBFormConstants.LOAN_DESCRIPTION, project);
		// controlWidgetsMap.put(NPMBFormConstants.MOBILE_NO, alternateNo);


		setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_PERSONAL_VALIDATORS, personalValidators);
		setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_EDUCATION_VALIDATORS, educationValidators);
		setupValidator(controlWidgetsMap, NPMBFormConstants.NPMB_FAMILY_VALIDATORS, familyValidators);
	}

	private void setupValidator(HashMap<String, BSTextWidget> controlWidgetsMap,
			HashMap<String, FormValidator[]> validationSet, ArrayList<BSControlValidator> targetValidatorList) {
		for (String formKey : validationSet.keySet()) {
			FormValidator[] fieldValidator = validationSet.get(formKey);
			BSTextWidget inputField = controlWidgetsMap.get(formKey);
			for (FormValidator v : fieldValidator)
				if (v.equals(FormValidator.MANDATORY))
					inputField.markRequired();
			BSLoanFieldValidator submitValidator = new BSLoanFieldValidator(fieldValidator, inputField, formKey,
					errorMap, true);
			targetValidatorList.add(submitValidator);
		}
	}



	public HashMap<String, String> getFormData() {
		HashMap<String, String> result = new HashMap<String, String>();
		result.put(NPMBFormConstants.SURNAME, surname.getValue());
		result.put(NPMBFormConstants.FIRST_NAME, firstName.getValue());
		result.put(NPMBFormConstants.TEL_NO, phone.getValue());
		result.put(NPMBFormConstants.EMAIL, email.getValue());
		result.put(NPMBFormConstants.BVN, bvn.getValue());

		//sme fields
		result.put(NPMBFormConstants.COMPANY_NAME, compName.getValue());
		result.put(NPMBFormConstants.COMPANY_REG, compNo.getValue());
		result.put(NPMBFormConstants.COMPANY_ADDY, compAddress.getValue());
		result.put(NPMBFormConstants.COMPANY_WEBSITE, compWeb.getValue());
		result.put(NPMBFormConstants.HEARD_ABOUT, heardAbout.getValue());
		result.put(NPMBFormConstants.BIZ_SECTOR, bizSector.getValue());
		
		//loan fields
		result.put(NPMBFormConstants.LOAN_AMOUNT, loanAmount.getValue());
		result.put(NPMBFormConstants.LOAN_TYPE, loanType.getValue());
		result.put(NPMBFormConstants.LOAN_DESCRIPTION, project.getValue());
		result.put(NPMBFormConstants.TENOR, tenor.getValue());
		result.put(NPMBFormConstants.REPAY_SOURCE, repay.getValue());
		
		return result;
	}

	public void setFormData(HashMap<String, String> result) {
		surname.setValue(result.get(NPMBFormConstants.SURNAME));
		firstName.setValue(result.get(NPMBFormConstants.FIRST_NAME));
		phone.setValue(result.get(NPMBFormConstants.TEL_NO));
		email.setValue(result.get(NPMBFormConstants.EMAIL));
		bvn.setValue(result.get(NPMBFormConstants.BVN));
		setDeclarationAuthor(result);
		//sme fields
		compName.setValue(result.get(NPMBFormConstants.COMPANY_NAME));
		compNo.setValue(result.get(NPMBFormConstants.COMPANY_REG));
		compAddress.setValue(result.get(NPMBFormConstants.COMPANY_ADDY));
		heardAbout.setValue(result.get(NPMBFormConstants.HEARD_ABOUT));
		bizSector.setValue(result.get(NPMBFormConstants.BIZ_SECTOR));
		compWeb.setValue(result.get(NPMBFormConstants.COMPANY_WEBSITE));
		//loan fields
		loanAmount.setValue(result.get(NPMBFormConstants.LOAN_AMOUNT));
		loanType.setValue(result.get(NPMBFormConstants.LOAN_TYPE));
		project.setValue(result.get(NPMBFormConstants.LOAN_DESCRIPTION));
		tenor.setValue(result.get(NPMBFormConstants.TENOR));
		repay.setValue(result.get(NPMBFormConstants.REPAY_SOURCE));
	}

	public void enableEditing(boolean isEdit) {
		boolean enabled = isEdit;
		surname.setEnabled(enabled);
		firstName.setEnabled(enabled);
		phone.setEnabled(enabled);
		email.setEnabled(false);
		bvn.setEnabled(enabled);
		
		//sme fields
		compName.setEnabled(enabled);
		compNo.setEnabled(enabled);
		compAddress.setEnabled(enabled);
		heardAbout.setEnabled(enabled);
		bizSector.setEnabled(enabled);
		compWeb.setEnabled(enabled);
		
		//loan fields
		loanAmount.setEnabled(enabled);
		loanType.setEnabled(enabled);
		project.setEnabled(enabled);
		tenor.setEnabled(enabled);
		repay.setEnabled(enabled);
		
		enableButtons(enabled);
	}

	public void setDeclarationAuthor(HashMap<String, String> result) {
		String[] nameParts = {result.get(NPMBFormConstants.FIRST_NAME), result.get(NPMBFormConstants.SURNAME) };
		declarationAuthor.setText(FormUtils.getFullName(nameParts));
	}

	public void clear() {
		setFormData(new HashMap<String, String>());
		enableButtons(false);
	}

	public void setLoanID(String loanID) {
		clear();
		CustomerAppHelper.LOAN_MKT_SERVICE.getLoanApplicationWithLoanID(loanID, fetchFormDataCallback);
	}
}
