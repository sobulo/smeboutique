package com.fertiletech.anchorsite.client.gui.feed;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class FeedC extends Composite{

	private static FeedCUiBinder uiBinder = GWT.create(FeedCUiBinder.class);

	interface FeedCUiBinder extends UiBinder<Widget, FeedC> {
	}

	public FeedC() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
