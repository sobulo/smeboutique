/**
 * 
 */
package com.fertiletech.anchorsite.server.entities;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import javax.persistence.Id;
import javax.persistence.PrePersist;

import com.fertiletech.anchorsite.shared.NPMBFormConstants;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Parent;
import com.googlecode.objectify.annotation.Serialized;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */

@Cached
@Unindexed
public class MortgageApplicationFormData {
	/**
	 * TODO
	 * key was initially an auto-generated long id. This led to a lot of unnecassary queries in codebase
	 * These should be optimized later for efficiency but until volumes kick up, we live with old queries
	 */
	@Id
	String key;
	
	@Indexed boolean submitted;
	@Indexed Date lastModified;
	
	@Parent Key<SalesLead> parentKey;
	
	@Serialized
	HashMap<String, String> formData;
	

	private final static String FORM_KEY = "form";
	
	//empty constructor needed for objectify
	MortgageApplicationFormData() {}
	
	MortgageApplicationFormData(Key<SalesLead> pKey, HashMap<String, String> data)
	{
		this(pKey, data, FORM_KEY);
	}
	
	protected MortgageApplicationFormData(Key<SalesLead> pKey, HashMap<String, String> data, String formKey)
	{
		this.parentKey = pKey;
		this.formData = data;
		this.submitted = false;
		this.key = formKey;
	}

	public HashMap<String, String> getFormData() {
		return formData;
	}

	void setFormData(HashMap<String, String> formData, boolean userSubmmited) {
		this.formData = formData;
		this.submitted = userSubmmited;
	}
	
	public static Key<MortgageApplicationFormData> getFormKey(Key<SalesLead> leadKey)
	{
		return getKey(leadKey, MortgageApplicationFormData.class, FORM_KEY);
	}
	
	protected final static <T extends MortgageApplicationFormData> Key<T>  getKey(Key<SalesLead> leadKey, Class<T> classType, String keyName)
	{
		return new Key<T>(leadKey, classType, keyName);
	}
	
	
	public Key<? extends MortgageApplicationFormData> getObjectifyKey()
	{
		return getKey(parentKey, this.getClass(), getKeyString());
	}
	
	public Key<? extends MortgageApplicationFormData> getKey()
	{
		return getFormKey(parentKey);
	}
	
	protected String getKeyString()
	{
		return key;
	}
	
	public Key<SalesLead> getParentKey() {
		return parentKey;
	}

	public boolean isSubmmited()
	{
		return submitted;
	}
	
	@PrePersist
	void lastModification()
	{
		lastModified = lastModified;
	}
	
	public Date getTimeStamp()
	{
		return lastModified;
	}
	
	MortgageApplicationFormData makeCopy(Key<SalesLead> lk)
	{
		String[] fieldsToBlank = {NPMBFormConstants.LOAN_AMOUNT, NPMBFormConstants.TENOR};
		HashSet<String> fieldsToBlankSet = new HashSet<String>(fieldsToBlank.length);
		MortgageApplicationFormData copy = new MortgageApplicationFormData();
		copy.parentKey = lk;
		HashMap<String, String> newData = new HashMap<String, String>();
		for(String key : fieldsToBlank)
			fieldsToBlankSet.add(key);
		for(String key : formData.keySet())
			if(!fieldsToBlankSet.contains(key))
				newData.put(key, formData.get(key));
		copy.formData = newData;
		
		return copy;
	}
}

