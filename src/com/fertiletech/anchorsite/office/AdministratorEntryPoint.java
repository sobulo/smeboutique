package com.fertiletech.anchorsite.office;

import com.google.gwt.core.client.EntryPoint;

public class AdministratorEntryPoint implements EntryPoint {
	@Override
	public void onModuleLoad() {
		new Showcase();
	}
}
