/**
 * 
 */
package com.fertiletech.anchorsite.apply.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface GeneralImageBundle extends ClientBundle{
	
	@Source("logo.png")
	ImageResource logoSmall();
	@Source("pattern.png")
	ImageResource downloadPattern();
	@Source("pattern2.png")
	ImageResource alternateDownloadPattern();
}
