package com.fertiletech.anchorsite.client.gui.menu;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class Swec extends Composite{
	
	private static AboutUsUiBinder uiBinder = GWT.create(AboutUsUiBinder.class);

	interface AboutUsUiBinder extends UiBinder<Widget, Swec> {
	}

	public Swec() {
		initWidget(uiBinder.createAndBindUi(this));		
	}

}
