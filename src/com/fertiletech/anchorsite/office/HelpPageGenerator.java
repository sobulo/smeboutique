package com.fertiletech.anchorsite.office;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTML;

public class HelpPageGenerator {
	//MODULE NAMES
	public final static String HELP_WELCOME_URL = "http://intranet.smeboutique.com";
	public final static String HELP_ADMIN_URL =  HELP_WELCOME_URL + "/admin"; 
	
	static HTML help;
	static
	{
		//setup help
		help = new HTML();
		help.setSize("95%", "500px");
		help.addStyleName("helpWatermark");
	}
	
	
	public static HTML getHelpWidget(String baseUrl, String moduleName)
	{
		String url = baseUrl + "/" + moduleName.replace(" ", "").toLowerCase();
		openNewWindow(moduleName, url);
		help.setHTML("<p>If you do not see the help window it may be due to your browser preventing popups.<br/> You can visit the help page on SME Boutique's Intranet directly at: <a href='" + url + "'>" + url + "</a></p>");
		return help;
	}
	
	private static void openNewWindow(String name, String url) {
	    Window.open(url, name.replace(" ", "_"),
	           "menubar=no," + 
	           "location=false," + 
	           "resizable=yes," + 
	           "scrollbars=yes," + 
	           "status=no," + 
	           "dependent=true");
	}
	
	public static String getHistoryTokenWithArgs(Class<? extends ContentWidget> cw, String args)
	{
		String token = Showcase.getContentWidgetToken(cw) + "/" + args;
		return token;
	}
}
