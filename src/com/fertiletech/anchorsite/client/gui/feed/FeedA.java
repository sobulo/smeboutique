package com.fertiletech.anchorsite.client.gui.feed;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class FeedA extends Composite{

	private static FeedAUiBinder uiBinder = GWT.create(FeedAUiBinder.class);

	interface FeedAUiBinder extends UiBinder<Widget, FeedA> {
	}

	public FeedA() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
