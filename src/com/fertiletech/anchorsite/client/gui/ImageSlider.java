package com.fertiletech.anchorsite.client.gui;

import com.fertiletech.anchorsite.client.FadeAnimation;
import com.fertiletech.anchorsite.client.FadeAnimationAlertCallback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class ImageSlider extends Composite implements FadeAnimationAlertCallback{

	private static ImageSliderUiBinder uiBinder = GWT
			.create(ImageSliderUiBinder.class);

	interface ImageSliderUiBinder extends UiBinder<Widget, ImageSlider> {
	}
	
	int largestScreenSizeSoFar = 1200;
		
	@UiField
	SimplePanel imageSlot;
	private final int STAY_DURATION = 12000;
	private final int FADE_DURATION = 1700;
	private int currentSlide;
	private Integer targetSlide = null;
	private FadeAnimation animator;
	private Image[] slideImages;
	boolean useCallBack = false;
	Timer playThread;
	
	public ImageSlider() {
		initWidget(uiBinder.createAndBindUi(this));
		slideImages = new Image[ImageDestinationManager.IMAGE_DESTINATION_MAP.size()];
		
		int i = 0;
		for(String imageName : ImageDestinationManager.IMAGE_DESTINATION_MAP.keySet())
		{
			slideImages[i] = new Image(GWT.getModuleBaseForStaticFiles() + "images/" + imageName);
			slideImages[i].setAltText(imageName);
			i++;
		}
		
		animator = new FadeAnimation(imageSlot.getElement(), this);
		
		//starting slide
		currentSlide = 0;
		imageSlot.add(slideImages[currentSlide]);
		animator.fade(100, 1);
		
		setupWindowSizing();
		
		//timer is used to transition
		play();
	}
	
	public void setupWindowSizing()
	{
		Window.addResizeHandler(new ResizeHandler() {
			
			@Override
			public void onResize(ResizeEvent event) {
				resizeMe();
			}
		});
		resizeMe();
	}
	
	public void resizeMe() //scales image within 1200-1600, and only in increases
	{
		int pixelWidth = Math.min(1600, Window.getClientWidth());
		if(pixelWidth < largestScreenSizeSoFar)
			pixelWidth = largestScreenSizeSoFar;
		else
			largestScreenSizeSoFar = pixelWidth;
		
		//configure for mobile device
		String userAgent = Window.Navigator.getUserAgent();
		String[] mobileTypes = {"Android","webOS","iPhone","iPad","iPod","BlackBerry","BB","PlayBook",
				"IEMobile","Windows Phone","Kindle","Silk","Opera Mini"};

		
		boolean isMobile = false;
		for(String s: mobileTypes)
		{
			if(userAgent.contains(s))
			{
				isMobile = true;
				break;
			}
		}
		
		if(isMobile)
			pixelWidth = Window.getClientWidth();
		
		
		int  pixelHeight = (int) (pixelWidth / 1600.0 * 590);
		for( Image slide : slideImages)
		{
			slide.setWidth(pixelWidth + "px");
			slide.setHeight(pixelHeight + "px");
			if(isMobile)
				slide.addStyleName("addMenuBuffer");
		}
		
		
	}
	
	public String getCurrentImageName()
	{
		return slideImages[currentSlide].getAltText();
	}
	
	//fade out
	private void changeSlide()
	{
		useCallBack = true;
		animator.fade(FADE_DURATION, 0.33);
	}

	//fade into the current slide
	@Override
	public void fadeFinished() {
		if(useCallBack)
		{
			if(targetSlide == null) //auto play
				currentSlide = (currentSlide + 1) % slideImages.length;
			else
			{
				currentSlide = targetSlide;
				targetSlide = null;
			}
			imageSlot.clear();
			imageSlot.add(slideImages[currentSlide]);
			useCallBack = false;
			animator.fade(FADE_DURATION, 1.0);
		}
	}
	
	public void move(boolean forward)
	{
		if(useCallBack) //ignore request as there is a pending fade in the queue
			return;
		
		if(forward)
			targetSlide = (currentSlide + 1) % slideImages.length;
		else
		{
			targetSlide = currentSlide - 1;
			if(targetSlide < 0)
				targetSlide = slideImages.length-1;
		}
		
		changeSlide();
	}
	
	public void selectImage(int imageIdx)
	{
		targetSlide = imageIdx;
	}
	
	public void play()
	{
		pause(); //in case we're currently playing
		
		playThread = new Timer() {
			@Override
			public void run() {
				changeSlide();
			}
		};
		playThread.scheduleRepeating(STAY_DURATION);
	}
	
	public void pause()
	{
		//this will not cancel any queued up fades but stops additions to the fade queue
		if(playThread != null)
			playThread.cancel();
		playThread = null;
	}
}
