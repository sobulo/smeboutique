package com.fertiletech.anchorsite.client;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Widget;
 
public class Fader {
 
    private static final String STYLE_DISPLAY = "display";
 
    private static final String DISPLAY_NONE = "none";
 
    private static final String DISPLAY_DEFAULT = "";
 
    private static final BigDecimal timesPerSecond = BigDecimal.valueOf(25);
 
    private static Map<Widget, Timer> timersMap = new HashMap<Widget, Timer>();
 
    private static void cancelTimers(final Widget w) {
        if (timersMap.containsKey(w)) {
            timersMap.get(w).cancel();
            timersMap.remove(w);
        }
    }
 
    private static boolean isBlank(String str)
    {
    	return (str == null || str.length() == 0);
    }
 
    public static void fadeIn(final Widget w, final int animationDuration) {
        cancelTimers(w);
 
        boolean wasVisible = w.isVisible();
        // Show the element - Let the Browser choose the proper display value (like in jQuery <img src="http://s0.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1129645325g" alt=":)" class="wp-smiley"> 
        w.getElement().getStyle().setVisibility(Visibility.HIDDEN);
        w.getElement().getStyle().setProperty(STYLE_DISPLAY, DISPLAY_DEFAULT);
 
        String opacityStr = w.getElement().getStyle().getOpacity();
        if (isBlank(opacityStr)) {
            if (wasVisible) {
                w.getElement().getStyle().setOpacity(1);
            } else {
                w.getElement().getStyle().setOpacity(0);
            }
        }
 
        w.getElement().getStyle().setVisibility(Visibility.VISIBLE);
 
        if (!isBlank(w.getElement().getStyle().getOpacity())
            && ((Double.valueOf(w.getElement().getStyle().getOpacity()))
                .doubleValue() != 1)) {
 
            final BigDecimal propertyIncrement =
                BigDecimal.valueOf(1).divide(
                    BigDecimal
                        .valueOf(animationDuration)
                        .divide(BigDecimal.valueOf((double) 1000), 5,
                            RoundingMode.HALF_UP).multiply(timesPerSecond), 5,
                    RoundingMode.HALF_UP);
            // Increase the Opacity
            Timer fadeInTimer = new Timer() {
                @Override
                public void run() {
                    String opacityStr =
                        isBlank(w.getElement().getStyle().getOpacity()) ? "1"
                            : w.getElement().getStyle().getOpacity();
                    boolean increased = false;
                    if (isBlank(opacityStr)) {
                        try {
                            BigDecimal opacity = new BigDecimal(opacityStr);
                            if (opacity.compareTo(new BigDecimal(1)) < 0) {
                                // increment is full property value / number of seconds / times of increase per second
                                w.getElement()
                                    .getStyle()
                                    .setOpacity(
                                        opacity.add(propertyIncrement)
                                            .doubleValue());
                                increased = true;
                            }
                        } catch (NumberFormatException nfe) {
                            // fallback to hiding
                        }
                    }
                    if (!increased) {
                        w.getElement().getStyle().setOpacity(1);
                        cancel();
                        timersMap.remove(w);
                    }
                }
            };
 
            timersMap.put(w, fadeInTimer);
 
            // Increase value "timesPerSecond" times per Second
            fadeInTimer.scheduleRepeating((BigDecimal.valueOf(1000).divide(
                timesPerSecond, 0, RoundingMode.HALF_UP)).intValue());
        }
 
    }

 
    public static void fadeOut(final Widget w, final int animationDuration) {
        cancelTimers(w);
 
        if (w.isVisible()) {
 
            final BigDecimal propertyIncrement =
            //                BigDecimal.valueOf(1).divide(BigDecimal.valueOf(animationDuration).divide(BigDecimal.valueOf(1000)).multiply(timesPerSecond));
                BigDecimal.valueOf(1).divide(
                    BigDecimal
                        .valueOf(animationDuration)
                        .divide(BigDecimal.valueOf((double) 1000), 5,
                            RoundingMode.HALF_UP).multiply(timesPerSecond), 5,
                    RoundingMode.HALF_UP);
 
            Timer fadeOutTimer = new Timer() {
 
                @Override
                public void run() {
                    String opacityStr =
                        isBlank(w.getElement().getStyle().getOpacity()) ? "1"
                            : w.getElement().getStyle().getOpacity();
                    boolean decreased = false;
                    if (isBlank(opacityStr)) {
                        try {
                            BigDecimal opacity = new BigDecimal(opacityStr);
                            if (opacity.compareTo(propertyIncrement) > 0) {
                                w.getElement()
                                    .getStyle()
                                    .setOpacity(
                                        opacity.subtract(propertyIncrement)
                                            .doubleValue());
                                decreased = true;
                            }
                        } catch (NumberFormatException nfe) {
                            // fallback to hiding
                        }
                    }
                    if (!decreased) {
                        w.getElement().getStyle()
                            .setProperty(STYLE_DISPLAY, DISPLAY_NONE);
                        cancel();
                        timersMap.remove(w);
                    }
                }
            };
 
            timersMap.put(w, fadeOutTimer);
 
            // Increase value "timesPerSecond" times per Second
            fadeOutTimer.scheduleRepeating((BigDecimal.valueOf(1000).divide(
                timesPerSecond, 0, RoundingMode.HALF_UP)).intValue());
        }
    }
}
