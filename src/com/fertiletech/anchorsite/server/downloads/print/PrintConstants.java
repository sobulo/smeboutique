package com.fertiletech.anchorsite.server.downloads.print;

import java.util.HashMap;

import com.fertiletech.anchorsite.shared.NPMBFormConstants;

public class PrintConstants {
	public static HashMap<String, String> PDFMappings = new HashMap<String, String>();
	static
	{
		PDFMappings.put(NPMBFormConstants.ID_NUM, "Loan Application ID");
	}
}
