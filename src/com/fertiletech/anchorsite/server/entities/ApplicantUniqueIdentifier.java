/**
 * 
 */
package com.fertiletech.anchorsite.server.entities;

import javax.persistence.Id;

import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class ApplicantUniqueIdentifier {
	@Id
	String email;

	ApplicantUniqueIdentifier() {}
	
	ApplicantUniqueIdentifier(String email)
	{
		this.email = email.toLowerCase();
	}

	Key<ApplicantUniqueIdentifier> getKey()
	{
		return getKey(this.email);
	}
	
	public static Key<ApplicantUniqueIdentifier> getKey(String email)
	{
		return new Key<ApplicantUniqueIdentifier>(ApplicantUniqueIdentifier.class, email.toLowerCase());
		
	}
}
