package com.fertiletech.anchorsite.server.downloads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fertiletech.anchorsite.server.entities.ApplicationParameters;
import com.fertiletech.anchorsite.server.entities.EntityConstants;
import com.fertiletech.anchorsite.server.login.LoginHelper;
import com.fertiletech.anchorsite.server.tasks.TaskConstants;
import com.fertiletech.anchorsite.shared.DTOConstants;
import com.fertiletech.anchorsite.shared.TableMessage;
import com.fertiletech.anchorsite.shared.TableMessageHeader;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;


public class GenericExcelDownload extends HttpServlet{
    private final static String SESS_PREFIX = "fertiletech.";
    private final static HashMap<String, String> LICENSED_USERS;
    
    static
    {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(DTOConstants.APP_PARAM_REVIEWER);
		ApplicationParameters appObj = ObjectifyService.begin().get(pk);
		LICENSED_USERS = appObj.getParams();
    }
    
    public void saveAuditTrail(String html, String user)
    {
		Key<ApplicationParameters> pk = ApplicationParameters.getKey(DTOConstants.APP_PARAM_REVIEWER);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			ApplicationParameters appObj = ofy.get(pk);
			appObj.addToAuditTrail(html, user);
			ofy.put(appObj);
			
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
    	String id = req.getParameter(TaskConstants.EXCEL_DOWN_KEY_PARAM);
    	String user = LoginHelper.getLoggedInUser(req); 	
    	
    	if(id == null)
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Unable to process request. Please ensure this page is accessed via a link" +
    				" recently generated from the a table listing. If problem persists contact Addosser IT</font></b>");
    		return;
    	}
    	HttpSession sess = req.getSession();
    	ArrayList<TableMessage> data = (ArrayList<TableMessage>) sess.getAttribute(getGenericDownloadSessionName(id));
    	int approvedRows = downloadApproved(user);
    	if(approvedRows < data.size() - 1) //-1 for header
    	{
    		res.setContentType("text/html");
    		res.getOutputStream().println("<b><font color='red'>Insufficient privileges. Please ensure this page is accessed via a link" +
    				" recently generated from the a table listing. If problem persists contact Addosser IT to review your excel quota</font></b>");
    		return;    		
    	}
		String fileName = EntityConstants.DATE_FORMAT.format(new Date(Long.valueOf(id)));
		fileName = fileName.replace(" ", "-") + ".xls";    	
        //download as ssheet to client comp
        ExcelDownloadHelper.doDownload(res, fileName, data);
        //mark audit trail
        saveAuditTrail("<b style='color:#17762b>Excel download:" + (data.size()-1) + " rows.</b>", user);
    }
    
    public static int downloadApproved(String id)
    {
    	try
    	{
    		return Integer.valueOf(LICENSED_USERS.get(id));
    	}
    	catch(RuntimeException Ex)
    	{
    		return 0;
    	}
    }
    
    public static String getGenericExcelDownloadLink(List<TableMessage> data, TableMessageHeader header, HttpServletRequest req)
    {
    	HttpSession sess = req.getSession();
        String id = String.valueOf(new Date().getTime());
        ArrayList<TableMessage> cachedData = new ArrayList<TableMessage>(data.size() + 1);
        cachedData.add(header);
        cachedData.addAll(data);
        sess.setAttribute(getGenericDownloadSessionName(id), cachedData);
        String user = LoginHelper.getLoggedInUser(req);
        int approvedRows = downloadApproved(user);
        
        if(approvedRows >= data.size())
        	return "<b>DOWNLOAD: </b><a href='" + "http://" + req.getHeader("Host") + "/excel/generic?" + 
        		TaskConstants.EXCEL_DOWN_KEY_PARAM + "=" + id + "'>" + user + " click here for excel download</a>";         
        else
        	return "<b style='red'>" + user + " with approval limit of " + approvedRows + 
        			" rows is not approved for this excel download with " + data.size() + "rows." +
        			" Contact Addosser IT to increase your approval limits</b>";
    }
    
	public static String getGenericDownloadSessionName(String id)
	{
		return SESS_PREFIX + id + "exldown";
	}    
}
