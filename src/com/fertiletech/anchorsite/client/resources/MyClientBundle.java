package com.fertiletech.anchorsite.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;


public interface MyClientBundle extends ClientBundle {

	ImageResource logo();

	//news feed controls
	ImageResource left();
	ImageResource right();
	
	//player controls for scrolling through background images 
	ImageResource play();
	ImageResource forward();
	ImageResource pause();
	ImageResource back();
}

