package com.fertiletech.anchorsite.client.gui.feed;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class FeedB extends Composite{

	private static FeedBUiBinder uiBinder = GWT.create(FeedBUiBinder.class);

	interface FeedBUiBinder extends UiBinder<Widget, FeedB> {
	}

	public FeedB() {
		initWidget(uiBinder.createAndBindUi(this));
	}
}
