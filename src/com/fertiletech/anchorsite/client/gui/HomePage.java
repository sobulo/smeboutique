package com.fertiletech.anchorsite.client.gui;


import com.fertiletech.anchorsite.client.gui.menu.AboutBank;
import com.fertiletech.anchorsite.client.gui.menu.ApplyNow;
import com.fertiletech.anchorsite.client.gui.menu.CustomerTestimonials;
import com.fertiletech.anchorsite.client.gui.menu.Services;
import com.fertiletech.anchorsite.client.gui.menu.Swec;
import com.fertiletech.anchorsite.client.resources.MyClientBundle;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimpleLayoutPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class HomePage extends ResizeComposite{

	private static HomePageUiBinder uiBinder = GWT
			.create(HomePageUiBinder.class);

	interface HomePageUiBinder extends UiBinder<Widget, HomePage> {
	}

	@UiField
	TabLayoutPanel feed;
	
	@UiField
	FocusPanel imageArea;
	
	@UiField
	Image goLeft;
	
	@UiField
	Image goRight;
	
	@UiField
	SimpleLayoutPanel logoSlot;
	
	@UiField
	SimplePanel menuSlot;
	@UiField
	SimplePanel hoverSlot;	

	private PopupPanel hoverDisplay;
	ImageSlider slider;
	
	Timer dullTimer = null;
	
	private static MyClientBundle primaryBundle = GWT.create(MyClientBundle.class);
	
	public HomePage(ImageSlider sliderReference) {
		initWidget(uiBinder.createAndBindUi(this));
		this.slider = sliderReference;
		hoverDisplay = new PopupPanel();
		imageArea.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				imageArea.setFocus(false);
				//jump to the selected image
				String urlKey = slider.getCurrentImageName();
				String url = ImageDestinationManager.IMAGE_DESTINATION_MAP.get(urlKey);
				Window.open(url, "_blank", "");
			}
		});
		feed.setAnimationDuration(1500);
		ClickHandler h = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(event.getSource() ==  goRight)
					moveSlide(true);
				else
					moveSlide(false);
			}
		};
		goLeft.addClickHandler(h);
		goRight.addClickHandler(h);
		setupHeaderPanel();
		menuSlot.add(getMenuItems());
		hoverSlot.setLayoutData(menuSlot.getLayoutData());
	}
	
	private void setDullStyle(final Image arrow, boolean isAdd)
	{
		String style = "dullAnimation";
		if(isAdd)
		{
			arrow.addStyleName(style);
			dullTimer = new Timer() {
				
				@Override
				public void run() {
					setDullStyle(arrow, false);
					dullTimer = null;
				}
			};
			dullTimer.schedule(20*1000);
		}
		else
		{
			if(dullTimer != null)
			{
				dullTimer.cancel();
				dullTimer = null;
			}
			arrow.removeStyleName(style);
		}
	}
	
	private void  moveSlide(boolean isForward)
	{
		feed.setAnimationVertical(false);
		int i = feed.getSelectedIndex();
		if(isForward)
		{
			if(i == feed.getWidgetCount() - 1) //can't move forward
			{
				setDullStyle(goLeft, true);
				return;
			}
			i++;
		}
		else
		{
			if(i == 0)
			{
				setDullStyle(goRight, true);
				return;
			}
			i--;
		}		
		feed.selectTab(i);
		//remove style on both arrows, only one probably has it but no harm in calling on both
		setDullStyle(goLeft, false);
		setDullStyle(goRight, false);
	}
	
	private FlowPanel getMenuItems()
	{
		String baseUrl = "http://info.smeboutique.com/";
		String[] labels = {"Apply Now", "About Us", "Services" ,"Testimonials" , 
				"SWEC", "FAQs", "Contact Us"};
		String[] href = {"http://www.smeboutique.com/apply/", "about-us", "services", "testimonials", 
						 "swec", "faqs", "contact-us"};
		FlowPanel container = new FlowPanel();
        
		//setup HTMLPanels to Display
		final Widget[] displays = new Widget[labels.length];
		displays[0] = new ApplyNow();
		displays[1] = new AboutBank();
		displays[2] = new Services();
		displays[3] = new CustomerTestimonials();
		displays[4] = new Swec();
		displays[5] = null;
		displays[6] = null;
		
		
		//setup popup
        //hoverDisplay.setGlassEnabled(true);
        hoverDisplay.addStyleName("popupStyle");
        hoverDisplay.setAnimationEnabled(true);
        hoverDisplay.setAutoHideEnabled(false);
        hoverDisplay.setAutoHideOnHistoryEventsEnabled(true);
        
        FlowPanel contentArea = new FlowPanel();
        final SimplePanel display = new SimplePanel();
        //display.addStyleName("popupSlot");

        contentArea.add(display);
        FocusPanel hoverInOut = new FocusPanel();
        hoverInOut.add(contentArea);
        hoverDisplay.add(hoverInOut);
        final int hoverWidth = 500;
        final int hoverHeight = 320;
        hoverDisplay.setPixelSize(hoverWidth, hoverHeight);
        hoverInOut.addMouseOutHandler(new MouseOutHandler() {
			
			@Override
			public void onMouseOut(MouseOutEvent event) {
				//int x = Math.abs(event.getRelativeX(hoverDisplay.getElement()));
				//if(x > 1)
					hoverDisplay.hide();
			}
		});
        
		//for(int i = 0; i < labels.length ; i++)
		for(int i = labels.length-1; i >=0 ; i--)
		{
			String url = null;
			if(href[i].startsWith("http"))
				url = href[i];
			else
				url = baseUrl + href[i];
			final Anchor a = new Anchor(labels[i], url);
			if( i > 0)
				a.setTarget("_blank");
			a.addStyleName("simpleAnchor");
			container.add(a);
			
			final Widget w = displays[i];
			a.addMouseOverHandler(new MouseOverHandler() {
				
				@Override
				public void onMouseOver(MouseOverEvent event) {
					if(w == null) 
						hoverDisplay.hide();
					else
					{
						display.clear();
						display.add(w);
						//hoverDisplay.center();
						hoverDisplay.showRelativeTo(hoverSlot);
					}

				}
			});
			
			a.addMouseOutHandler(new MouseOutHandler() {
				int threshold = 30;
				//do we really want to do anything with this
				@Override
				public void onMouseOut(MouseOutEvent event) {
					int y = Math.abs(event.getRelativeY(hoverDisplay.getElement()));
					if(y > threshold || w == null)
						hoverDisplay.hide();
				}
			});
		}
		
		return container;
	}

	private void setupHeaderPanel()
	{
		//setup header children
		final Image logo = new Image(primaryBundle.logo());
		final Image play = new Image(primaryBundle.play());
		final Image pause = new Image(primaryBundle.pause());
		final Image forward = new Image(primaryBundle.forward());
		final Image backward = new Image(primaryBundle.back());
		final FocusPanel playPause = new FocusPanel(pause);
		backward.setStyleName("playerButton");
		forward.setStyleName("playerButton");
		playPause.setStyleName("playerButton");		
		logo.setTitle("SME Boutique Logo. Click Me!"); //for search engines

		//setup the header panel
		final DockLayoutPanel headerPanel = new DockLayoutPanel(Unit.PX);
		final FlowPanel playerControls = new FlowPanel();
		playerControls.addStyleName("playerControls");
		playerControls.add(backward);
		playerControls.add(playPause);
		playerControls.add(forward);
		
		final int PLAYER_WIDTH = 150;
		headerPanel.addWest(logo, 95);
		headerPanel.addWest(playerControls, PLAYER_WIDTH);

		headerPanel.setWidgetSize(playerControls, 0);
		headerPanel.forceLayout();
		headerPanel.setWidgetSize(playerControls, PLAYER_WIDTH);
		headerPanel.animate(3000);
		
		
		//styling
		
		final Image[] currentlyShowing = {pause};
		//setup click handlers
		play.setTitle("Click to resume playing");
		pause.setTitle("Click to pause");
		forward.setTitle("Next slide");
		backward.setTitle("Previous slide");
		playPause.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(currentlyShowing[0] == play) //logic duplicated below, hacky
				{
					play.removeFromParent();
					playPause.add(pause);
					currentlyShowing[0] = pause;
					slider.play();
				}
				else
				{
					pause.removeFromParent();
					playPause.add(play);
					currentlyShowing[0] = play;
					slider.pause();
				}
				playPause.setFocus(false);
			}
		});
		
		//hacky that currentlyShowing code is duplicated
		forward.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(currentlyShowing[0] == pause) //duplicate logic, hacky
				{
					pause.removeFromParent();
					playPause.add(play);
					currentlyShowing[0] = play;
					slider.pause();
					
				}
				slider.move(true);	
			}
		});
		
		backward.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(currentlyShowing[0] == pause)
				{
					pause.removeFromParent();
					playPause.add(play);
					currentlyShowing[0] = play;
					slider.pause();
				}
				slider.move(false);
			}
		});
				
		logo.addClickHandler(new ClickHandler() {
			final String[] toolTip = {"Click to hide SME Boutique player controls", 
					"Click to show SME Boutique player controls"};
			
			boolean visible = true;
			@Override
			public void onClick(ClickEvent event) {
				setWidgetLayout();
				headerPanel.forceLayout();
				visible = !visible;
				logo.setTitle(toolTip[visible?0:1]);
				setWidgetLayout();				
				headerPanel.animate(500);
			}
			
			private void setWidgetLayout()
			{
				headerPanel.setWidgetSize(playerControls, visible?PLAYER_WIDTH:0);
				//headerPanel.setWidgetSize(backward, visible?30:0);
				//headerPanel.setWidgetSize(forward, visible?30:0);
			}
		});
		logoSlot.add(headerPanel);
	}	
}