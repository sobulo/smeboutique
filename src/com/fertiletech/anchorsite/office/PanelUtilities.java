/**
 * 
 */
package com.fertiletech.anchorsite.office;

import com.fertiletech.anchorsite.base.LoginService;
import com.fertiletech.anchorsite.base.LoginServiceAsync;
import com.fertiletech.anchorsite.base.MortgageService;
import com.fertiletech.anchorsite.base.MortgageServiceAsync;
import com.fertiletech.anchorsite.office.util.SimpleDialog;
import com.google.gwt.core.client.GWT;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class PanelUtilities {
	public final static SimpleDialog infoBox = new SimpleDialog("<center><b style='color:green'>INFO</b></center>", true);
	public final static SimpleDialog errorBox = new SimpleDialog("<center><b style='color:red'>Error!</b></center>", true);

    final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);

    public static MortgageServiceAsync getLoanMktService()
    {
    	return LOAN_MKT_SERVICE;
    }
    
    public static LoginServiceAsync getLoginService()
    {
    	return READ_SERVICE;
    }	
}
