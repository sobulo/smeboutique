/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.anchorsite.apply;

import java.util.HashSet;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.extras.animate.client.ui.constants.Animation;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;

import com.fertiletech.anchorsite.apply.forms.FileIploadPanel;
import com.fertiletech.anchorsite.apply.forms.MortgageApplicationFormPanel;
import com.fertiletech.anchorsite.apply.forms.MortgageDownloadPanel;
import com.fertiletech.anchorsite.apply.forms.MortgageStatus;
import com.fertiletech.anchorsite.apply.images.GeneralImageBundle;
import com.fertiletech.anchorsite.base.GUIConstants;
import com.fertiletech.anchorsite.base.LoginService;
import com.fertiletech.anchorsite.base.LoginServiceAsync;
import com.fertiletech.anchorsite.base.MortgageService;
import com.fertiletech.anchorsite.base.MortgageServiceAsync;
import com.fertiletech.anchorsite.shared.oauth.ClientUtils;
import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.geolocation.client.Geolocation;
import com.google.gwt.geolocation.client.Geolocation.PositionOptions;
import com.google.gwt.geolocation.client.Position;
import com.google.gwt.geolocation.client.PositionError;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Segun Razaq Sobulo
 * 
 *         IMPORTANT: only use package access or private access in this class.
 *         This is to prevent unnecessary loading of certain gwt files in parts
 *         of the app that are public
 * 
 *         TODO run gwt code splitting analytic tools to explicitly confirm
 *         which classes get loaded by public sections of the app
 */
public class CustomerAppHelper {

	private static int indexCounter = 0;
	public final static int APPLICATION_FORM_IDX = indexCounter++;
	public final static int PRINT_FORM_IDX = indexCounter++;
	public final static int UPLOAD_IDX = indexCounter++;
	public final static int STATUS_TABLE_IDX = indexCounter++;

	private static HyperlinkedPanel[] appPanels = new HyperlinkedPanel[indexCounter];

	public final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
	public final static MortgageServiceAsync LOAN_MKT_SERVICE = GWT.create(MortgageService.class);
	final static GeneralImageBundle generalImages = GWT.create(GeneralImageBundle.class);
	private final static String APP_PAGE_URL_PREFIX = GWT.getHostPageBaseURL();
	final static String APP_PAGE_URL;
	final static int TOKEN_ARGS_PARAM_IDX = 0;
	final static int TOKEN_ARGS_RECENTLOAN_IDX = 1;
	final static int TOKEN_ARGS_EMAIL_IDX = 2;

	static {
		if (!GWT.isProdMode()) // true if dev mode
			APP_PAGE_URL = APP_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
		else
			APP_PAGE_URL = APP_PAGE_URL_PREFIX;
	}

	static HyperlinkedPanel[] initializePanels() {
		GWT.log("loading static block");
		// welcome panel

		appPanels[APPLICATION_FORM_IDX] = new HyperlinkedPanel() {

			private Hyperlink link;
			private MortgageApplicationFormPanel displayPanel;
			private WelcomeJumper tempDisplay;

			@Override
			public Hyperlink getLink() {
				if (link == null) {
					link = new Hyperlink("Edit Application", NameTokens.APPLY);
				}
				return link;
			}

			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
				String loanID = getLoanID(args);
				if (loanID == null) {
					if (tempDisplay == null)
						tempDisplay = new WelcomeJumper();
					if (isOps)
						tempDisplay.setHTML(
								"No appllication specified. Use the M.A.P portal to create/edit applications. You will now"
										+ " be redirected to the welcome page of the public portal");
					else
						tempDisplay.setHTML("No application found. "
								+ "You will be redirected to the welcome page where you can start an application");

					return tempDisplay;
				}
				if (displayPanel == null) {
					displayPanel = new MortgageApplicationFormPanel();
				}
				displayPanel.clear();

				displayPanel.setLoanID(loanID);
				return displayPanel;
			}
		};

		appPanels[PRINT_FORM_IDX] = new HyperlinkedPanel() {

			private Hyperlink link;
			private MortgageDownloadPanel displayPanel;

			@Override
			public Hyperlink getLink() {
				if (link == null) {
					link = new Hyperlink("Download/Print", NameTokens.PRINT);
				}
				return link;
			}

			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
				if (displayPanel == null) {
					displayPanel = new MortgageDownloadPanel();
				}
				displayPanel.setLoanID(getLoanID(args), isOps, isLoggedIn);
				return displayPanel;
			}
		};

		appPanels[UPLOAD_IDX] = new HyperlinkedPanel() {

			private Hyperlink link;
			private FileIploadPanel displayPanel;

			@Override
			public Hyperlink getLink() {
				if (link == null) {
					link = new Hyperlink("Upload Documents", NameTokens.UPLOAD);
				}
				return link;
			}

			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
				if (displayPanel == null) {
					displayPanel = new FileIploadPanel();
				}
				displayPanel.setLoanID(getLoanID(args), isOps, isLoggedIn);
				return displayPanel;
			}
		};

		appPanels[STATUS_TABLE_IDX] = new HyperlinkedPanel() {

			private Hyperlink link;
			private MortgageStatus displayPanel;

			@Override
			public Hyperlink getLink() {
				if (link == null) {
					link = new Hyperlink("Status", NameTokens.STATUS);
				}
				return link;
			}

			@Override
			public Widget getPanelWidget(boolean isLoggedIn, boolean isOps, String[] args) {
				if (displayPanel == null) {
					displayPanel = new MortgageStatus();
				}
				displayPanel.setLoandID(getLoanID(args), isOps, isLoggedIn);
				return displayPanel;
			}
		};

		return appPanels;
	}

	static Hyperlink[] getMenuItems() {
		GWT.log("calling gettree");

		Hyperlink[] result = new Hyperlink[appPanels.length];

		// welcome menu item
		for (int i = 0; i < appPanels.length; i++)
			result[i] = appPanels[i].getLink();

		return result;
	}

	static Icon ico = new Icon();
	static Modal infoBox = new Modal();
	static HTML message = new HTML();

	static {
		ico.setSize(IconSize.TIMES3);
		HorizontalPanel temp = new HorizontalPanel();
		temp.setSpacing(10);
		temp.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		temp.add(ico);
		temp.add(message);
		temp.setSpacing(20);
		VerticalPanel container = new VerticalPanel();
		container.setSpacing(10);
		Button dismiss = new Button("OK");
		dismiss.setType(ButtonType.PRIMARY);
		dismiss.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				infoBox.hide();
			}
		});
		dismiss.setBlock(true);
		container.add(temp);
		container.add(dismiss);
		infoBox.add(container);
	}

	private static NotifySettings goGood, goBad;

	static {
		
		 goGood = NotifySettings.newSettings(); goBad = NotifySettings.newSettings();
		 goGood.setType(NotifyType.SUCCESS); goBad.setType(NotifyType.DANGER);
		 goGood.setAnimation(Animation.ZOOM_IN_LEFT,
		 Animation.ZOOM_OUT_RIGHT); goBad.setAnimation(Animation.TADA,
		 Animation.FADE_OUT);
		 goGood.setDelay(15000);
		 
	}

	public static void showErrorMessage(String error) {
		Notify.notify("","<div style='width:400px'>" + error + "</div>",IconType.WARNING,goBad);
	}

	public static void showInfoMessage(String info) {
		Notify.notify("", "<div style='width:400px'>" + info + "</div>", IconType.INFO_CIRCLE, goGood);
	}

	public static String getLoanID(String[] args) {
		return (args[TOKEN_ARGS_PARAM_IDX] == null ? args[TOKEN_ARGS_RECENTLOAN_IDX] : args[TOKEN_ARGS_PARAM_IDX]);
	}

	public static void setupCompanyOracle(SuggestBox suggestBox) {
		String[] companyListVals = {};
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) suggestBox.getSuggestOracle();
		for (int counter = 0; counter < companyListVals.length; counter++) {
			oracle.add(companyListVals[counter]);
		}
	}

	private static HashSet<String> longitudeCache = new HashSet<String>();

	public static void updateLatLngVals(final String loanId) {
		if (ClientUtils.NPMBUserCookie.getCookie().isOps())
			return; // ensure only applicant positions are tracked
		if (longitudeCache.contains(loanId))
			return;
		if (Geolocation.isSupported()) {
			PositionOptions opts = new Geolocation.PositionOptions();
			opts.setHighAccuracyEnabled(true);
			opts.setTimeout(15 * 1000); // wait 15 seconds
			Geolocation.getIfSupported().getCurrentPosition(new Callback<Position, PositionError>() {

				@Override
				public void onSuccess(Position result) {
					CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, result.getCoordinates().getLatitude(),
							result.getCoordinates().getLongitude(), null);
					longitudeCache.add(loanId);
				}

				@Override
				public void onFailure(PositionError reason) {
					// Window.alert("Error getting gps coordinates: " +
					// reason.getMessage());
					CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
				}
			}, opts);
		} else {
			CustomerAppHelper.READ_SERVICE.saveMapAttachment(loanId, null, null, null);
			longitudeCache.add(loanId);
		}
	}
	
	public static void markLocked(Widget container, boolean locked)
	{
		if(locked)
		{
			container.removeStyleName("markOpened");
			container.addStyleName("markLocked");
		}
		else
		{
			container.removeStyleName("markLocked");
			container.addStyleName("markOpened");
		}
	}	

	
	//http://www.sitepoint.com/google-analytics-track-javascript-ajax-events/ for how to use
	public static native void trackEvent(String category, String action, String label) /*-{
		$wnd._gaq.push([ '_trackEvent', category, action, label ]);
	}-*/;

	public static native void trackEvent(String category, String action, String label, int intArg) /*-{
		$wnd._gaq.push([ '_trackEvent', category, action, label, intArg ]);
	}-*/;

	public static native void trackPageview(String url) /*-{
		$wnd._gaq.push([ '_trackPageview', url ]);
	}-*/;
}
