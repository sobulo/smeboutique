/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.anchorsite.server.login;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class LoginConstants
{
    public final static String COMPANY_DOMAIN = "@smeboutique.com";
    public final static String FTBS_DOMAIN = "@fertiletech.com";
    public final static String ADDOSSER_DOMAIN = "@addosser.com";
    public final static HashMap<String, String> ALLOWED_NAMESPACES = new HashMap<String, String>();

}
