package com.fertiletech.anchorsite.apply;

import org.gwtbootstrap3.client.ui.gwt.FlowPanel;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;

public class WelcomeJumper extends Composite{
	String title = "<H1>Oops!</H1>";
	HTML display = new HTML();
	Anchor staticJump = new Anchor("Click here to get started if you're not redirected" +
			" in a few seconds", GWT.getHostPageBaseURL());
	
	WelcomeJumper()
	{
		FlowPanel container = new FlowPanel();
		//container.setSpacing(20);
		container.add(display);
		container.add(staticJump);
		container.addStyleName("prettyWrap");
		initWidget(container);
		staticJump.addStyleName("buffer");
		
	}
	
	public void setHTML(String msg)
	{
		display.setHTML(title + msg + "<br/><hr/>");
		new Timer() {
			
			@Override
			public void run() {
				Window.Location.replace(GWT.getHostPageBaseURL());
				
			}
		}.schedule(7000);
	}

}
