package com.fertiletech.anchorsite.server;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SwecRedirect extends HttpServlet{

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		res.sendRedirect("http://info.smeboutique.com/swec/");
	}
}
