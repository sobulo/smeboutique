package com.fertiletech.anchorsite.office.content.shell;

import com.fertiletech.anchorsite.office.CWArguments;
import com.fertiletech.anchorsite.office.ContentWidget;
import com.fertiletech.anchorsite.office.HelpPageGenerator;
import com.fertiletech.anchorsite.office.Showcase;
import com.fertiletech.anchorsite.office.content.ApplicationStatus;
import com.fertiletech.anchorsite.shared.TableMessage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class StatusCW extends ContentWidget implements CWArguments{
	String savedId;
	ApplicationStatus statusPanel;
	public StatusCW() {
		super("Application Status", "Manage an individual application form. Features include viewing, editing, and downloading submitted applications");
	}

	@Override
	public void setParameterValues(String args) {
		if(statusPanel == null)
			savedId = args;
		else
			statusPanel.setLoanID(args);
	}

	@Override
	public Widget onInitialize() {
		statusPanel = new ApplicationStatus();
		if(savedId != null)
			statusPanel.setLoanID(savedId);
		return statusPanel;
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(StatusCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}
	
	static class StatusJumper implements ValueChangeHandler<TableMessage> 
	{

		@Override
		public void onValueChange(ValueChangeEvent<TableMessage> event) {
			History.newItem(Showcase.getContentWidgetToken(StatusCW.class) + "/" + event.getValue().getMessageId());
		}
	}
}
