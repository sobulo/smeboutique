package com.fertiletech.anchorsite.office.content.shell;

import com.fertiletech.anchorsite.office.ContentWidget;
import com.fertiletech.anchorsite.office.HelpPageGenerator;
import com.fertiletech.anchorsite.office.content.StatusChartPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;

public class StatusChartCW extends ContentWidget{

	public StatusChartCW() {
		super("Status Chart", "View counts of applications based on their current status");
		// TODO Auto-generated constructor stub
	}

	@Override
	public Widget onInitialize() {
		return new StatusChartPanel();
	}

	@Override
	protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
		GWT.runAsync(StatusChartCW.class, new RunAsyncCallback() {

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess() {
				callback.onSuccess(onInitialize());
			}
		});	
	}

	@Override
	protected String getHelpUrl() {
		return HelpPageGenerator.HELP_WELCOME_URL;
	}

}
