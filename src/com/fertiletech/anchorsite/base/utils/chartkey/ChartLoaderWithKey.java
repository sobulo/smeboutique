package com.fertiletech.anchorsite.base.utils.chartkey;

import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.apiloader.ApiLoaderOptions;

//motivated by https://www.latecnosfera.com/2016/06/google-maps-api-error-missing-keymap-error-solved.html
public class ChartLoaderWithKey extends ChartLoader{
	private static final String API_NAME_COPY = "visualization";
	
	public ChartLoaderWithKey(ChartPackage... packages) 
	{
		super(packages);
	}
	
	public void loadApiWithKey(Runnable callback, String key) {
		ChartPackage[] packages = getPackages();
		String[] packagesArray = new String[packages.length];
		for (int i = 0; i < packages.length; i++) {
			packagesArray[i] = packages[i].getName();
		}
		ApiLoaderOptions options = ApiLoaderOptions.create();
		options.setPackages(packagesArray);
		//if(key != null)
		//	options.setOtherParams("key="+key);
		String language = getLanguage();
		if (language != null) {
			options.setLanguage(language);
		}
		ApiLoaderWithKey.loadApi(API_NAME_COPY, getVersion(), callback, options, key);           
	}	
}
