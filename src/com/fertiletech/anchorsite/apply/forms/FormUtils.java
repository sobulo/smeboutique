package com.fertiletech.anchorsite.apply.forms;


import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.gwtbootstrap3.client.ui.SuggestBox;

import com.fertiletech.anchorsite.apply.formwidgets.BSListBox;
import com.fertiletech.anchorsite.apply.formwidgets.BSTextWidget;
import com.fertiletech.anchorsite.apply.formwidgets.CurrencyBox;
import com.fertiletech.anchorsite.apply.formwidgets.FlexPanel;
import com.fertiletech.anchorsite.shared.TableMessageHeader;
import com.fertiletech.anchorsite.shared.TableMessageHeader.TableMessageContent;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.MultiWordSuggestOracle;


public class FormUtils {
	public static void setupFlexPanel(FlexPanel panel, String[][] headers, Integer noRows)
	{
		TableMessageHeader h = new TableMessageHeader(headers[0].length);
		for(int i = 0; i < headers[0].length; i++)
		{
			h.setText(i, headers[0][i], TableMessageContent.TEXT);
			int width = Integer.valueOf(headers[1][i]);
			h.setFormatOption(i, String.valueOf((width-1)));
			h.setGroupName(i, headers[4][i]);
		}
		panel.initHeader(h, headers[2][0], headers[2][1], noRows);
	}

	public static void setupListBox(BSTextWidget<String> lbWrapper, Iterable<String> options) {
		BSListBox tempBox = (BSListBox) lbWrapper.getFieldWidget();
		setupListBox(tempBox, options);
	}
	
	
	public static void setupListBox(BSListBox tempBox, Iterable<String> options) {
		if(tempBox.getItemCount() > 0)
			tempBox.clear();
		for (String opt : options) {
			tempBox.addItem(opt);
		}
	}
	
	public static void setupListBox(BSTextWidget<String> tempBox, String[] options) {
		setupListBox(tempBox, Arrays.asList(options));
		
	}
	
	public static Double getAmount(BSTextWidget<String> currencyBox) {
		CurrencyBox cb = (CurrencyBox) currencyBox.getFieldWidget();
		return cb.getAmount();
	}	
	
	
	public static void setupListBox(BSListBox lb, int start, int finish, String singular, String plural)
	{	
		for(int i = start; i <= finish ; i++)
		{
			if(i == 1)
				lb.addItem(i + " " + singular, String.valueOf(i));
			else
				lb.addItem(i + " " + plural, String.valueOf(i));
		}
	}
	
	public static void setupListBox(BSTextWidget<String> lb, int start, int finish, String singular, String plural)
	{
		BSListBox box = (BSListBox) lb.getFieldWidget();
		setupListBox(box, start, finish, singular, plural);
	}
	
	public static void setOtherListCombo(BSTextWidget<String> lb, BSTextWidget<String> otherField, String value)
	{
		BSListBox box = (BSListBox) lb.getFieldWidget();
		setOtherListCombo(box, otherField, value);
	}	
	
	public static void setOtherListCombo(BSListBox lb, BSTextWidget<String> otherField, String value)
	{
		if(value == null)
		{
			otherField.clear();
			otherField.setEnabled(false);
			lb.setSelectedIndex(0);
			return;
		}
		
		boolean useOther = true;
		for(int i = 0; i < lb.getItemCount() - 1; i++)
			if(lb.getValue(i).equalsIgnoreCase(value))
				useOther = false;
		
		if(!useOther)
		{
			lb.setValue(value);
			otherField.clear();
			otherField.setEnabled(false);
		}
		else
		{
			lb.setSelectedIndex(lb.getItemCount() - 1);
			otherField.setValue(value);
			otherField.setEnabled(true);
		}
			
	}
	
	public static void initOtherListCombo(BSTextWidget<String> lb,BSTextWidget<String> otherField)
	{
		initOtherListCombo((BSListBox) lb.getFieldWidget(), otherField);
	}
	
	public static void initOtherListCombo(final BSListBox lb,final BSTextWidget<String> otherField)
	{
		lb.addValueChangeHandler(new ValueChangeHandler<String>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				if(event.getValue().equals(lb.getValue(lb.getItemCount() - 1)))
				{
					otherField.setEnabled(true);
				}
				else
				{
					otherField.clear();
					otherField.setEnabled(false);
				}			
			}
		});
	}
	
	public static String getFullName(String[] nameParts)
	{
		StringBuilder result = new StringBuilder();
		for(int i = 0; i < nameParts.length; i++)
			if(nameParts[i] == null)
				continue;
			else
				result.append(nameParts[i].trim()).append(" ");
		return result.toString().trim();
	}
	
	public static void setupSuggestBox(BSTextWidget<String> box, String[] vals)
	{
		GWT.log("Called Type Ahead for " + box.getTitle());
		List<String> values = Arrays.asList(vals);
		GWT.log("Converted to array ok, fetching type ahead");
		SuggestBox h = (SuggestBox) box.getFieldWidget();
		MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) h.getSuggestOracle();
		oracle.addAll(values);
		GWT.log("datasets done");
	}
	
	public static void setupSuggestBox(BSTextWidget<String> box, Set<String> set)
	{

		SuggestBox h = (SuggestBox) box.getFieldWidget();
		((MultiWordSuggestOracle) h.getSuggestOracle()).addAll(set);
	}	
}
