package com.fertiletech.anchorsite.shared;

import java.util.HashMap;


public class NPMBFormConstants {
	
	//personal fields
	public static String ID = "AW-Portal-ID";
	public static String ID_NUM = "AW-Portal-NO";
	public static String ID_STATE = "AW-State";
	public static String ID_EDIT = "AW-Write";
	
	//personal
	public static String SURNAME = "Last Name"; 
	public static String FIRST_NAME = "First Name"; 
	public static String TEL_NO = "Telephone No.";  //k
	public static String EMAIL = "Email Address";
	public static String BVN = "Bank Verification No.";
	
	//sme fields
	public static String COMPANY_NAME = "Company Name";
	public static String COMPANY_REG = "Company Registration No.";
	public static String COMPANY_WEBSITE = "Company Website";
	public static String COMPANY_ADDY = "Office Address";
	public static String BIZ_SECTOR = "Business Sector";
	public static String HEARD_ABOUT = "Referred By";
	
	//loan fieds
	public static String LOAN_AMOUNT = "Loan Amount";
	public static String TENOR = "Tenor";
	public static String REPAY_SOURCE = "Repayment Source";
	public static String LOAN_TYPE = "Loan Type";
	public static String LOAN_DESCRIPTION = "Request Summary";
	
	
	//declaration/signature by user upon completion
	public static String DECLARATION = "I hereby declare that all information provided in this application are correct and" +
			" that all copies of documents I will submit are authentic. I understand that SME Boutique will verify the information supplied. " +
			"I therefore agree that any material misstatement/falsification of information discovered renders my application null and void.";
	
	public static int MOOC_IDX = 0;

	
	//SME Boutique business categories
	public final static String[] BIZ_CATEGORIES = {"Financial Services","Digital & Creative", 
			"Information technology", "Engineering", "Telecoms","Pharmaceutical & Health",
			"Manufacturing", "Education", "Trade & Commerce", "Fashion", "Retail", 
			"Professional services", "Agriculture"};
	
	//loan types
	public final static String[] LOAN_TYPES = {"Working Capital Loan", "Bridge Finance", 
			"LPO Finance", "Invoice Discounting", "Asset Acquisition Finance",
			"Import Duty Finance", "Female Entrepreneur Loan", "Bespoke Credit",
			"Group Loan", "Overdraft", "Widows Umbrella"};
	
	public final static String[] TABLE_DESCRIPTIONS = {};
	public final static String[] FILE_TYPES = {"Passport Photo", "Bank Statement", "CAC Document", "Utility Bill"};

	//form validators for welcome page
	public static HashMap<String, FormValidator[]> WELCOME_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_PERSONAL_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_FAMILY_VALIDATORS = new HashMap<String, FormValidator[]>();
	public static HashMap<String, FormValidator[]> NPMB_EDUCATION_VALIDATORS = new HashMap<String, FormValidator[]>();

	static
	{
    	FormValidator[] mandatoryOnly = {FormValidator.MANDATORY};
    	FormValidator[] emailOnly = {FormValidator.EMAIL};
    	FormValidator[] phoneOnly = {FormValidator.PHONE};
    	FormValidator[] mandatoryPlusPhone = {FormValidator.MANDATORY, FormValidator.PHONE};
    	FormValidator[] mandatoryPlusEmail = {FormValidator.MANDATORY, FormValidator.EMAIL};
    	FormValidator[] mandatoryPlusParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_INTEGER};
    	FormValidator[] mandatoryPlusDateParsesOk = {FormValidator.MANDATORY, FormValidator.DATE_OLD_ENOUGH};
    	FormValidator[] mandatoryPlusFutureDateOk = {FormValidator.MANDATORY, FormValidator.DATE_IS_FUTURE};    	
    	FormValidator[] mandatoryPlusLoanParsesOk = {FormValidator.MANDATORY, FormValidator.PARSES_DOUBLE};
    	FormValidator[] decimalParsesOk = {FormValidator.PARSES_DOUBLE};	
    	FormValidator[] atLeastOneParsesOk = {FormValidator.PARSES_INTEGER_1};		
    	FormValidator[] atLeastFiveParsesOk = {FormValidator.PARSES_INTEGER_5};	
    	FormValidator[] validBVN = {FormValidator.BVN};
    	FormValidator[] validWeb = {FormValidator.WEB};

    	
    	//welcome page
    	WELCOME_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	WELCOME_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	WELCOME_VALIDATORS.put(SURNAME, mandatoryOnly);
    	WELCOME_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	
    	//personal validators
    	NPMB_PERSONAL_VALIDATORS.put(EMAIL, mandatoryPlusEmail);
    	NPMB_PERSONAL_VALIDATORS.put(TEL_NO, mandatoryPlusPhone);
    	NPMB_PERSONAL_VALIDATORS.put(SURNAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(FIRST_NAME, mandatoryOnly);
    	NPMB_PERSONAL_VALIDATORS.put(BVN, validBVN);

    	//sme validators
    	NPMB_EDUCATION_VALIDATORS.put(COMPANY_NAME, mandatoryOnly);
    	NPMB_EDUCATION_VALIDATORS.put(COMPANY_WEBSITE, validWeb);
    	
    	//loan validators
    	NPMB_FAMILY_VALIDATORS.put(LOAN_AMOUNT, mandatoryPlusLoanParsesOk);
    	NPMB_FAMILY_VALIDATORS.put(REPAY_SOURCE, mandatoryOnly);
    	NPMB_FAMILY_VALIDATORS.put(LOAN_DESCRIPTION, mandatoryOnly);

	}
	
	public static Boolean[] getSubmissionValues(TableMessage result)
	{
		Boolean appSubmitted, guard1Submitted, guard2Submitted;
		
		long subVal = Math.round(result.getNumber(DTOConstants.LOAN_KEY_IDX));
		if( subVal == 1)
			appSubmitted = true;
		else 
			appSubmitted = false;
		
		Boolean[] subs = new Boolean[1];
		subs[0] = appSubmitted;
		return subs;
	}	
	

}
