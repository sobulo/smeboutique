package com.fertiletech.anchorsite.client.resources.menu;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface MenuBundle extends ClientBundle{
	
	//apply now menu icons
	ImageResource edit_app();
	ImageResource download_print();
	ImageResource upload_docs();
	ImageResource loan_approved();
	
	//about us graphic
	ImageResource bank();
	ImageResource sme_small();
	
	//services icons
	ImageResource advisory();
	ImageResource deposit();
	ImageResource business_loans();
	
	//testimonials icons
	ImageResource cbn();
	ImageResource addosser();
	ImageResource sme();
	ImageResource boi();
	
	//swec icons
	ImageResource woman();
	ImageResource swec();	
	ImageResource calendar();	

}
