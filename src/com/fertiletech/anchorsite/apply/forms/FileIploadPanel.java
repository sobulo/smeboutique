package com.fertiletech.anchorsite.apply.forms;

import java.util.Date;
import java.util.List;

import org.gwtbootstrap3.client.ui.ListBox;
import org.gwtbootstrap3.client.ui.html.Paragraph;
import org.gwtbootstrap3.extras.bootbox.client.Bootbox;
import org.gwtbootstrap3.extras.bootbox.client.callback.ConfirmCallback;

import com.fertiletech.anchorsite.apply.CustomerAppHelper;
import com.fertiletech.anchorsite.apply.table.TablesView;
import com.fertiletech.anchorsite.base.GUIConstants;
import com.fertiletech.anchorsite.base.MyAsyncCallback;
import com.fertiletech.anchorsite.shared.DTOConstants;
import com.fertiletech.anchorsite.shared.NPMBFormConstants;
import com.fertiletech.anchorsite.shared.TableMessage;
import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Widget;

public class FileIploadPanel extends Composite implements ClickHandler,
		FormPanel.SubmitCompleteHandler {

	@UiField
	Button submit;
	@UiField
	Button refresh;
	@UiField
	FileUpload selectFile;
	@UiField
	FormPanel uploadForm;
	//@UiField
	//HTML status;
	@UiField
	TablesView fileList;
	@UiField
	Hidden loanID;
	@UiField
	Paragraph finePrint;
	@UiField
	Hidden fileType;
	@UiField
	ListBox typeSelector;
	@UiField
	FlowPanel container;
	
	private static FileIploadPanelUiBinder uiBinder = GWT
			.create(FileIploadPanelUiBinder.class);

	interface FileIploadPanelUiBinder extends UiBinder<Widget, FileIploadPanel> {
	}

	AsyncCallback<List<TableMessage>> refreshCallback = new AsyncCallback<List<TableMessage>>() {

		@Override
		public void onFailure(Throwable caught) {
			CustomerAppHelper
					.showErrorMessage("File list refresh failed. Error was: "
							+ caught.getMessage());
			setFinePrint("color: red", "File list displayed may be stale. Try hitting the refresh button above or refreshing your browser");
		}

		@Override
		public void onSuccess(List<TableMessage> result) {
			boolean alreadyInitialized = fileList.isInitialized();
			fileList.showTable(result);
			if (!alreadyInitialized) {
				fileList.addColumn(getColumn(), "View");
				fileList.addColumn(getDeleteColumn(), "Delete");
			}
			CustomerAppHelper
					.showInfoMessage("File list refreshed");
			setFinePrint("color: orange", "File list refreshed on: " + GUIConstants.DEFAULT_DATE_TIME_FORMAT.format(new Date()));
		}
	};	
	
	public FileIploadPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		for(String s : NPMBFormConstants.FILE_TYPES)
			typeSelector.addItem(s);
		//typeSelector.set
		typeSelector.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				fileType.setValue(typeSelector.getValue(typeSelector.getSelectedIndex()));
			}
		});
		
		submit.addClickHandler(this);
		uploadForm.addSubmitCompleteHandler(this);
		uploadForm.setMethod(FormPanel.METHOD_POST);
		uploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
		selectFile.setName("chooseFile");
		enableAll(false);
		
		refresh.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if(loanID.getValue()==null || loanID.getValue().trim().length() == 0)
				{
					CustomerAppHelper.showErrorMessage("No application selected. Aborting refresh");
					return;
				}				
				CustomerAppHelper.READ_SERVICE.getUploads(loanID.getValue(), refreshCallback);
			}
		});
	}

	private void enableAll(boolean enabled) {
		selectFile.setEnabled(enabled);
		refresh.setEnabled(enabled);
		submit.setEnabled(enabled);
		CustomerAppHelper.markLocked(container, !enabled);
	}

	private void clear() {
		fileList.clear();
		loanID.setName("dummy");
		loanID.setValue("");
		enableAll(false);
	}

	private void setFinePrint(String style, String message)
	{
		finePrint.setHTML("<span style='" + "'>" + message + "</span>");		
	}
	
	
	public void setLoanID(String newLoanID, boolean isOps, boolean isLoggedIn) {
		clear();
		if(!isLoggedIn)
		{
			String message = "Uploads disabled. Please sign in.";
			setFinePrint("color:grey", message);
			CustomerAppHelper.showErrorMessage(message);
			return;
		}
		if (newLoanID == null || newLoanID.trim().length() == 0)
		{
			String msg = "Uploads disabled. " + (isOps? "Select an application on M.A.P." : " Try refreshing your browser");
			CustomerAppHelper.showErrorMessage(msg);
			setFinePrint("color:red", msg);
			return;
		}
		if(isOps)
		{
			setFinePrint("background-color: green; color:white;", "NOTEWORTHY: Uploads done here are visible to the applicant." +
					" Use the Drive picker on M.A.P. status page to attach files that are private to SME Boutique staff.");
		}
		loanID.setName(DTOConstants.GCS_LOAN_ID_PARAM);
		fileType.setName(DTOConstants.GCS_FILE_NAME_PARAM);
		fileType.setValue(typeSelector.getSelectedValue());
		loanID.setValue(newLoanID);
		enableAll(true);
	}

	@Override
	public void onSubmitComplete(SubmitCompleteEvent event) {
		setFinePrint("color: green", "Upload Succesful!");
		CustomerAppHelper.READ_SERVICE.getUploads(loanID.getValue(), refreshCallback);
		CustomerAppHelper.showInfoMessage("Upload Successful!");
	}

	@Override
	public void onClick(ClickEvent event) {
		AsyncCallback<String> callback = new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				CustomerAppHelper
						.showErrorMessage("Uploads disabled, error message is: "
								+ caught.getMessage());
				setFinePrint("color: red", "Uploads disabled. Error occured while uploading. Refresh your browser and try again");
				enableAll(true);
			}

			@Override
			public void onSuccess(String result) {
				if (result == null)
					return;
				
				setFinePrint("color: orange", "Almost done uploading, file list should refresh automatically in a few seconds"
						+ ". Hit the 'Refresh List' button above if this is not the case.");
				uploadForm.setAction(result);
				enableAll(true);
				uploadForm.submit();
			}
		};
		if(selectFile.getFilename() == null || selectFile.getFilename().trim().length()==0)
		{
			CustomerAppHelper.showErrorMessage("Select a file to upload");
			return;
		}
		
		int typeIdx = typeSelector.getSelectedIndex();
		String selectedType = typeSelector.getValue(typeIdx);
		if(typeIdx == 0)
		{
			if(!selectFile.getFilename().toLowerCase().endsWith(".jpg") && !selectFile.getFilename().toLowerCase().endsWith(".jpeg"))
			{
				String msg = "Only JPG/JPEG picture format supported for " + selectedType + " Aborting upload.";
				CustomerAppHelper.showErrorMessage(msg);
				setFinePrint("color: red", msg);
				return;
			}
		}
		else if(!selectFile.getFilename().toLowerCase().endsWith(".pdf"))
		{
			String msg = "Only PDF file format supported for " + selectedType + " Aborting upload.";
			CustomerAppHelper.showErrorMessage(msg);
			setFinePrint("color: red", msg);
			return;
		}		
		enableAll(false);
		if(loanID.getValue()==null || loanID.getValue().trim().length() == 0)
		{
			CustomerAppHelper.showErrorMessage("No application selected. Aborting upload");
			return;
		}
		setFinePrint("text-decoration: blink", "uploading file, please wait ...");
		fileType.setValue(selectedType);
		CustomerAppHelper.READ_SERVICE.getUploadUrl(callback);
	}

	private Column<TableMessage, SafeHtml> getColumn() {
		SafeHtmlCell cell = new SafeHtmlCell();
		Column<TableMessage, SafeHtml> urlColumn = new Column<TableMessage, SafeHtml>(
				cell) {

			@Override
			public SafeHtml getValue(TableMessage object) {
				SafeHtmlBuilder sb = new SafeHtmlBuilder();
				String url = object.getMessageId();
				sb.appendHtmlConstant("<a target='_blank' href='" + url
						+ "'>Download</a>");
				return sb.toSafeHtml();
			}
		};
		return urlColumn;
	}
	
	private Column<TableMessage, String> getDeleteColumn()
	{
		ButtonCell removeAttachment = new ButtonCell();
		Column<TableMessage, String> invCol = new Column<TableMessage, String>(
				removeAttachment) {

			@Override
			public String getValue(TableMessage object) {
				return "Delete File";
			}
		};
		
		invCol.setFieldUpdater(new FieldUpdater<TableMessage, String>() {
								

			@Override
			public void update(int index, final TableMessage object,
					String value) {
				final String[] parts = object.getText(1).split("/", 2);
				
				final MyAsyncCallback<String> editAttachCallback = new MyAsyncCallback<String>() {

					@Override
					public void onFailure(Throwable caught) {
						CustomerAppHelper.showErrorMessage("Looks like attachment removal failed. " 
									+ caught.getMessage());
						setFinePrint("color: red", "Failed to delete attachment. Refresh your browser and try again");
					}

					@Override
					public void onSuccess(String result) {
						CustomerAppHelper.showInfoMessage(result);
						setFinePrint("color: brown", result);
						CustomerAppHelper.READ_SERVICE.getUploads(parts[0], refreshCallback);

					}

					@Override
					protected void callService(AsyncCallback<String> cb) {
						enableWarning(false);
						CustomerAppHelper.READ_SERVICE.deleteUpload(object.getText(1), cb);
					}
				};
				
				Bootbox.confirm("The file" + ", " + object.getText(0) + " will be permanently deleted from SME Boutique Records. Proceed?", new ConfirmCallback() {
					
					@Override
					public void callback(boolean result) {
						if(result)
							editAttachCallback.go("Deleting file " + parts[1]);
						else
							CustomerAppHelper.showInfoMessage("Aborted delete of " + parts[1]);
					}
				});
			}
		});
		return invCol;
	}
}
