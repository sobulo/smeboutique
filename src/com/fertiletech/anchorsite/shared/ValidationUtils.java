/**
 * 
 */
package com.fertiletech.anchorsite.shared;


import com.google.gwt.regexp.shared.RegExp;

/**
 * @author Segun Razaq Sobulo
 */
public class ValidationUtils {
	
    private final static String REGEX_EMAIL = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    private final static String REGEX_NUMS_ONLY = "^\\d{6,}$";
    private final static String REGEX_BVN = "^\\d{11}$";
    private static final RegExp EMAIL_MATCHER = RegExp.compile(REGEX_EMAIL, "i");
    private static final RegExp PHONE_NUM_MATCHER = RegExp.compile(REGEX_NUMS_ONLY);
    private static final RegExp BVN_MATCHER = RegExp.compile(REGEX_BVN);
    private final static String REGEX_PHONE_REPLACE = "-|\\(|\\)";
    
    private final static RegExp WEBSITE_MATCHER = RegExp.compile("^(http://|https://)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$");

    
    public static boolean isValidEmail(String email)
    {
    	return EMAIL_MATCHER.exec(email) != null;
    }
    
    public static boolean isValidNum(String num)
    {
    	return PHONE_NUM_MATCHER.exec(num.replaceAll(REGEX_PHONE_REPLACE, "")) != null;
    }    
    
    public static boolean isValidBVN(String bvn)
    {
    	return BVN_MATCHER.exec(bvn.replaceAll(REGEX_PHONE_REPLACE, "")) != null;
    }
    
    public static boolean isValidWebsite(String web)
    {
    	return WEBSITE_MATCHER.exec(web) != null;
    }
}
